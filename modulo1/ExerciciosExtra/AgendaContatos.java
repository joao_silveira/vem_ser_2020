import java.util.*;
public class AgendaContatos {
    private HashMap<String, String> hashMapContatos;
    
    AgendaContatos(){
        this.hashMapContatos = new LinkedHashMap<>();
    }
    
    public void adicionar(String nome, String numero){
        hashMapContatos.put(nome, numero);
    }
    
    public String obter(String nome){
        return hashMapContatos.get(nome);
    }
    
    public String consutarPeloTelefone( String telefone ) {
        for ( HashMap.Entry<String, String> par : hashMapContatos.entrySet() ){
            if ( par.getValue().equals(telefone) ) {
                return par.getKey();
            }
        }
        return null;
    }
    
    public String csv() {
        StringBuilder builder = new StringBuilder();
        String separador = System.lineSeparator();
        for ( HashMap.Entry<String, String> par : hashMapContatos.entrySet() ) {
            String chave = par.getKey();
            String valor = par.getValue();
            String contato = String.format("%s, %s%s", chave, valor, separador);
            builder.append(contato);
        }
        return builder.toString();
    }

}
