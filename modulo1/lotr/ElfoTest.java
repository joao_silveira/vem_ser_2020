
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest {
    
    @Test
    public void elfoContadorDeInstancias() {
        Elfo elfoQualquer = new Elfo("legolas");
        ElfoNoturno elfoNoturno = new ElfoNoturno("legolas");
        ElfoVerde elfoVerde = new ElfoVerde("legolas");
        
        assertEquals(3, Elfo.getQtdInstancias());
    }
    
    @Test
    public void elfoDeveNascerCom2Flechas() {
        Elfo elfoQualquer = new Elfo("legolas");
        assertEquals(2, elfoQualquer.getQtdFlechas());
    }
    
    @Test
    public void atirarFlechaDevePerderFlechaAumentarXP() {
        Elfo elfoQualquer = new Elfo("Legolas", 45);
        Dwarf anao = new Dwarf("Malungrid");
        
        elfoQualquer.atirarFlecha(anao);
        
        assertEquals(44, elfoQualquer.getQtdFlechas());
        assertEquals(1, elfoQualquer.getExperiencia());
        
    }
    
    @Test
    public void atirar3FlechasTendo2Flechas() {
        Elfo elfoQualquer = new Elfo("Legolas", 2);
        Dwarf anao = new Dwarf("Malungrid");
        
        elfoQualquer.atirarFlecha(anao);
        elfoQualquer.atirarFlecha(new Dwarf("Remela"));
        elfoQualquer.atirarFlecha(new Dwarf("Malungrid"));
        
        assertEquals( 0, elfoQualquer.getQtdFlechas());
        assertEquals( 2, elfoQualquer.getExperiencia());
    }
    
    @Test
    public void atirarFlechaNoDwarf() {
        Elfo elfoQualquer = new Elfo("Rubeola", 2);
        Dwarf dwarfQualquer = new Dwarf("Remela");
        
        elfoQualquer.atirarFlecha( dwarfQualquer );
        
        assertEquals( 100.0 , dwarfQualquer.getVida(), 0.0001 );
        
        assertEquals( 1, elfoQualquer.getQtdFlechas());
        assertEquals( 1, elfoQualquer.getExperiencia());   
    }
}
