import java.util.*;

public class Mago extends Personagem {
    protected double mana;
    
    Mago(String nome){
        super(nome);
    }
    
    {
        this.vida = 70;
        this.mana = 70;
    }
    
    protected double getMana(){
        return this.mana;
    }
    
    protected void ganhaMana(double mana){
        this.mana += mana;
    }
    
    protected void perdeMana(double manaLoss){
        this.mana -= manaLoss;
    }
    
    protected boolean alvoValido(Personagem inimigo) {
        
        if(inimigo instanceof Elfo || inimigo instanceof Dwarf){ 
            return true;
        }
        return false;
    }
    
    //cada bola de fogo e o equivalente a 15 de mana
    public void jogarBolarDeFogo(Personagem inimigo){
        boolean podeJogar = getMana() >=15 ? true : false;
       
        if(podeJogar && alvoValido(inimigo)){
            this.perdeMana(15.0);
            inimigo.sofrerDano();
            aumentarXP();
        }
    }
    
    //A bola de fogo da dano em area nos primeiros Personagens do Exercito
    public void jogarBolarDeFogoEmExercito(ArrayList<Personagem> exercito){
        boolean podeJogar = getMana() >=15 ? true : false;
       
        if(podeJogar){
            for(int i = 0; i < 4; i++){
                exercito.get(i).sofrerDano();
            }
            this.perdeMana(15.0);
            
            aumentarXP();
        }
    }
    
    
    @Override
    public String imprimirNomeDaClasse(){
        return "mago";
    }
    
}
