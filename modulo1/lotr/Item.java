public class Item {
    //quantidade, descricao
    private String descricao;
    protected int quantidade;
    
    public Item(int quantidade, String descricao) {
        this.setQuantidade(quantidade);
        this.descricao = descricao;
    }
    
    public String getDescricao(){
        return this.descricao;
    }
    
    public int getQuantidade() {
        return this.quantidade;
    }
    
    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
}