
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest {
    @Test
    public void elfoNoturnoDeveGanhar3XpAoAtirarFlecha(){
        ElfoNoturno elfoNoturno = new ElfoNoturno("Ana Maria Braga");
        Dwarf dwarf = new Dwarf("Gafun");
        
        elfoNoturno.atirarFlecha(dwarf);
        
        assertEquals(elfoNoturno.getExperiencia(), 3, .1);
    }
    
    @Test
    public void elfoNoturnoDevePerder15HpAoAtirarFlecha(){
        ElfoNoturno elfoNoturno = new ElfoNoturno("Ana Maria Braga");
        Dwarf dwarf = new Dwarf("Gafun");
        
        elfoNoturno.atirarFlecha(dwarf);
        
        assertEquals(elfoNoturno.getVida(), 85.0, .1);
    }
    
    @Test
    public void elfoNoturnoDeveAtirar7FlechasEMorrer(){
        ElfoNoturno elfoNoturno = new ElfoNoturno("Ana Maria Braga");
        Dwarf dwarf = new Dwarf("Gafun");
        elfoNoturno.getInventario().obter(1).setQuantidade(1000);
        
        elfoNoturno.atirarFlecha(dwarf);
        elfoNoturno.atirarFlecha(dwarf);
        elfoNoturno.atirarFlecha(dwarf);
        elfoNoturno.atirarFlecha(dwarf);
        elfoNoturno.atirarFlecha(dwarf);
        elfoNoturno.atirarFlecha(dwarf);
        elfoNoturno.atirarFlecha(dwarf);
        
        assertEquals(elfoNoturno.getVida(), 0, .1);
    }
    
}
