import java.util.*;

public class PaginadorInventario {
    Inventario inventario;
    
    private int marcador = 0;
    
    public PaginadorInventario(Inventario inventario){
        this.inventario = inventario;
    }
    
    public void pular(int marcador){
        this.marcador = marcador > 0 ? marcador : 0;
    }
    
    public ArrayList<Item> limitar(int qtdItem){
        ArrayList<Item> itensPaginados = new ArrayList<>();
        int fim = this.marcador + qtdItem;
        
        for(int i = this.marcador; i < this.inventario.getItens().size(); i++){
            itensPaginados.add(this.inventario.obter(i));
        }
        
        return itensPaginados;
    } 
    
}
