import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest {
    
    @Test
    public void elfoDaLuzDevePerder21HpAoAtirarFlechaImparECurar10FlechaPar(){
        ElfoDaLuz calaquendi = new ElfoDaLuz("Ana Maria Braga");
        Dwarf dwarf = new Dwarf("Gafun");
        
        calaquendi.atacarComEspada(dwarf);
        
        assertEquals(calaquendi.getVida(), 79, .1);
        
        calaquendi.atacarComEspada(dwarf);
        
        assertEquals(calaquendi.getVida(), 89, .1);
        
    }
    
}
