
import java.util.*;

public class EstatisticasInventario {
    
    private Inventario inventario;
    
    public EstatisticasInventario(Inventario inventario){
        this.inventario = inventario;
    }

    //getItens getQuantidade
    
    public double calcularMedia(){
        
        if( this.inventario.getItens().isEmpty() ) {
            return Double.NaN;
        } 
        
        double totalSoma = 0;
        int totalItens = this.inventario.getItens().size();
        
        for(int i = 0 ; i < totalItens ; i++) {
            totalSoma += this.inventario.getItens().get(i).getQuantidade();
        }
        
        double media = totalSoma / totalItens;
        
        return media;
    }
    
    public int[] ordenaArray(int array[]){
        int[] arrayOrdenado = new int[array.length];
        
        //cp o array dinamico p um array estatico
        for(int i = 0 ; i<array.length - 1 ; i++){
            arrayOrdenado[i] = array[i];
        }
        
        for(int i = 0; i < arrayOrdenado.length -1 ; i++ ){
            for(int j = 0 ; j < arrayOrdenado.length - i - 1; j++) {
                if(arrayOrdenado[j] > arrayOrdenado[j+1]){
                    int aux = arrayOrdenado[j];
                    arrayOrdenado[j] = arrayOrdenado[j+1];
                    arrayOrdenado[j+1] = aux;
                }
            }
        }
        
        return arrayOrdenado;
    }
    
    public double calcularMediana(){
        if( this.inventario.getItens().isEmpty() ) {
            return Double.NaN;
        } 
        
        int[] itensQtd = new int[this.inventario.getItens().size()];
        
        for(int i = 0 ; i < this.inventario.getItens().size() ; i++) {
            itensQtd[i] = this.inventario.getItens().get(i).getQuantidade();
        }
        
        int[] arrayOrdenado = ordenaArray(itensQtd);
        
        if(arrayOrdenado.length % 2 == 0){
            int mediana = arrayOrdenado[arrayOrdenado.length/2] + arrayOrdenado[(arrayOrdenado.length/2) - 1];
            return mediana/2;
        } else {
            int mediana = arrayOrdenado[(arrayOrdenado.length/2) + 1];
            return mediana; 
        }
    }
    
    public int qtdItensAcimaDaMedia(){
        
        int qtdItensAcimaDaMedia = 0;
        double media = this.calcularMedia();
        
        for(int i = 0; i<this.inventario.getItens().size() ; i++){
            if(this.inventario.getItens().get(i).getQuantidade() > media){
                qtdItensAcimaDaMedia++;
            }
        }
        
        return qtdItensAcimaDaMedia;
    }
    
    
}
