import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class MagoBrancoTest {
 
    @Test
    public void deveGanharVidaEmAtaqueParENaoPerderManaEmAtaqueImpar(){
        MagoBranco Gandalf = new MagoBranco("Gandalf");
        Elfo Ariovaldo = new Elfo("Ariovaldo");
        
        Gandalf.jogarBolarDeFogo(Ariovaldo);
        
        //ataque impar da dano nao perde mana nem ganha vida
        assertEquals(Gandalf.getMana() , 70.0 , 0.1 );
        assertEquals(Gandalf.getVida() , 70.0 , 0.1 );
        assertEquals(Ariovaldo.getVida() , 80.0 , 0.1 );
        
        
        Gandalf.jogarBolarDeFogo(Ariovaldo);
        //ataque par da dano perde mana e ganha vida
        assertEquals(Gandalf.getMana() , 55.0 , 0.1 );
        assertEquals(Gandalf.getVida() , 75.0 , 0.1 );
        assertEquals(Ariovaldo.getVida() , 60.0 , 0.1 );
        
        
        Gandalf.jogarBolarDeFogo(Ariovaldo);
        //ataque impar da dano nao perde mana nem ganha vida
        assertEquals(Gandalf.getMana() , 55.0 , 0.1 );
        assertEquals(Gandalf.getVida() , 75.0 , 0.1 );
        assertEquals(Ariovaldo.getVida() , 40.0 , 0.1 );
        
        
    }
}
