import java.util.*;

public class MagoBranco extends Mago {
    private int ataqueContador;
    
    public MagoBranco(String nome){
        super(nome);
    }
    
    {
        ataqueContador = 0;
        this.qtdCura = 5;
    }
    
    private boolean ataquePar(){
        return ataqueContador % 2 == 0;
    }
    
    @Override
    public void jogarBolarDeFogo(Personagem inimigo){
        boolean podeJogar = this.getMana() >=15 ? true : false;
        
        if(podeJogar && alvoValido(inimigo)){
            ataqueContador++;
       
            if(ataquePar() ){
                this.perdeMana(15.0);
                this.curar();
            }
            
            inimigo.sofrerDano();
            aumentarXP();
    
        }
    }
    
    @Override
    public void jogarBolarDeFogoEmExercito(ArrayList<Personagem> exercito){
        boolean podeJogar = getMana() >=15 ? true : false;
        
        if(podeJogar){
            ataqueContador++;
            for(int i = 0; i < 4; i++){
                exercito.get(i).sofrerDano();
            }
            if(ataquePar() ){
                this.perdeMana(15.0);
                this.curar();
            }
            
            aumentarXP();
    
        }
    }
    
    
}
