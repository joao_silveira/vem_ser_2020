import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class InventarioTest {
    
    /*
    @Test
    public void criarInventarioSemQuantidadeInformada(){
        Inventario inventario = new Inventario();
        assertEquals(99,inventario.getItens().size());
    }
    
    
    @Test
    public void criarInventarioComQuantidadeInformada(){
        Inventario inventario = new Inventario(40);
        assertEquals(40, inventario.getItens().length);
    }
    */
   
    @Test
    public void adicionarItemNoInventario(){
        Inventario inventario = new Inventario();
        Item item = new Item(1,"arco");
        Item escudo = new Item(1,"escudo");
        
        inventario.adicionaItem(item);
        inventario.adicionaItem(escudo);
        
        assertEquals(item,inventario.getItens().get(0));
        assertEquals(escudo,inventario.getItens().get(1));
    }
    
    @Test
    public void adicionarItemNoInventarioEObter(){
        Inventario inventario = new Inventario();
        Item item = new Item(1,"arco");
        Item escudo = new Item(1,"escudo");
        
        inventario.adicionaItem(item);
        inventario.adicionaItem(escudo);
        
        assertEquals(item,inventario.obter(0));
        //assertEquals(escudo,inventario.getItens()[1]);
    }
    
    @Test
    public void adicionarItemNoInventarioERemover(){
        Inventario inventario = new Inventario();
        Item item = new Item(1,"arco");
        Item escudo = new Item(1,"escudo");
        
        inventario.adicionaItem(item);
        inventario.adicionaItem(escudo);
        assertEquals(item,inventario.obter(0));
        
        inventario.removerItem(0);
       
        assertEquals( 1, inventario.getItens().size());
    }
    
    @Test
    public void deveRetornarListaDeNomesDeItens(){
        Inventario inventario = new Inventario();
        inventario.adicionaItem(new Item(1,"arco"));
        inventario.adicionaItem(new Item(1,"flecha"));
        
        assertEquals("arco,flecha",inventario.getDescricoesItens());
    }
 
    @Test
    public void deveRetornarItemMaiorQtd(){
        Inventario inventario = new Inventario();
        inventario.adicionaItem(new Item(1,"arco"));
        inventario.adicionaItem(new Item(3,"flecha"));
        inventario.adicionaItem(new Item(5,"escudo"));
        
        assertEquals(5, inventario.getItemMaiorQtd().getQuantidade());
    }
 
    @Test
    public void deveBuscarItemPorDescricao(){
        Inventario inventario = new Inventario();
        
        Item arco = new Item(1,"arco");
        Item flecha = new Item(1,"flecha");
        Item escudo = new Item(1,"escudo");
        
        inventario.adicionaItem(arco);
        inventario.adicionaItem(flecha);
        inventario.adicionaItem(escudo);
        
        Item resultado = inventario.buscar("arco");
        assertEquals(arco, resultado);
        
    }
    
    @Test
    public void deveRetornarArrayInvertido() {
        Inventario inventario = new Inventario();
        inventario.adicionaItem(new Item(1,"arco"));
        inventario.adicionaItem(new Item(1,"flecha"));
        
        assertEquals("arco,flecha",inventario.getDescricoesItens());
        
        ArrayList<Item> inverso = inventario.inverter();
        
        //assertEquals("arco,flecha", inverso.toString());
        //assertEquals("flecha,arco", inverso.toArray().toString().);
        
        
    }
    
    @Test
    public void deveOrdenarArrayListPorQtdItensASC() {
        Inventario inventario = new Inventario();
        
        Item arco = new Item(4,"arco");
        Item flecha = new Item(2,"flecha");
        Item escudo = new Item(7,"escudo");
        
        inventario.adicionaItem(arco);
        inventario.adicionaItem(flecha);
        inventario.adicionaItem(escudo);
        
        inventario.ordenarInventario();
        
        assertEquals("flecha,arco,escudo",inventario.getDescricoesItens());
    }
    
    
    @Test
    public void deveOrdenarArrayListPorQtdItensDESC() {
        Inventario inventario = new Inventario();
        
        Item arco = new Item(4,"arco");
        Item flecha = new Item(2,"flecha");
        Item escudo = new Item(7,"escudo");
        
        inventario.adicionaItem(arco);
        inventario.adicionaItem(flecha);
        inventario.adicionaItem(escudo);
        
        inventario.ordenarInventario(TipoOrdenacao.DESC);
        
        assertEquals("escudo,arco,flecha",inventario.getDescricoesItens());
    }
    
}
