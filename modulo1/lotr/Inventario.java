import java.util.*;

public class Inventario extends Object {
    ArrayList<Item> itens = new ArrayList<>();
    
    public ArrayList<Item> getItens(){
        return itens;
    }
    
    public Item buscar(String descricao) {
        for(int i = 0; i < itens.size() ; i++) {
            if( itens.get(i).getDescricao().equals(descricao) ){
                return itens.get(i);
            }
        }
        
        return null;
    }
    
    public ArrayList<Item> inverter(){
        ArrayList<Item> arrayInverso = new ArrayList<>();
        
        for(int i = itens.size()-1 ; i >= 0  ; i-- ){
            arrayInverso.add(this.itens.get(i));
        }
        return arrayInverso;
    }
    
    public void adicionaItem(Item item){
        itens.add(item);
    }
    
    public Item obter(int posicao) {
        if( posicao >= this.itens.size()){
            return null;
        }
        return this.itens.get(posicao);
    }
    
    public void removerItem(int posicao) {
        this.itens.remove(posicao);
    }
    
    public void removerItem(Item item) {
        this.itens.remove(item);
    }
    
    //exercicio 02
    public String getDescricoesItens(){
        String descricaoItens = new String();
        
        for (int i = 0; i < itens.size(); i++ ) {
            if (this.itens.get(i) != null ) {
                descricaoItens = descricaoItens + this.itens.get(i).getDescricao() + ",";
            }
        }
        
        
        descricaoItens = descricaoItens.substring(0, descricaoItens.length() - 1);
        return descricaoItens;
    }
    
    public Item getItemMaiorQtd() {
        int auxMaiorQtd = 0, indexDoMaior = 0;
        
        for(int i = 0; i < this.itens.size() ; i++) {
            if (this.itens.get(i).getQuantidade() > auxMaiorQtd) {
                indexDoMaior = i;
                auxMaiorQtd = this.itens.get(i).getQuantidade();
            }
        }
        
        return this.itens.get(indexDoMaior);
    }
    
    public void ordenarInventario(){
        Item aux;
        int quantidadeInventario = this.itens.size();
        
        for(int i = 0; i < quantidadeInventario ; i++) {
            for(int j = 0 ; j < quantidadeInventario - i - 1 ; j++) {
                if(this.obter(j).getQuantidade() > this.obter(j + 1).getQuantidade()){
                    aux = this.obter(j);
                    this.itens.set(j, this.obter(j + 1));
                    this.itens.set(j + 1, aux);
                }
            }
        }
        
    }
    
    public void ordenarInventarioDesc(){
        Item aux;
        int quantidadeInventario = this.itens.size();
        
        for(int i = 0; i < quantidadeInventario ; i++) {
            
            for(int j = 0 ; j < quantidadeInventario - i - 1 ; j++) {
        
                if(this.obter(j).getQuantidade() < this.obter(j + 1).getQuantidade()){
                    aux = this.obter(j);
                    this.itens.set(j, this.obter(j + 1));
                    this.itens.set(j + 1, aux);
                }
            }
        }
    }
    
    public void ordenarInventario(TipoOrdenacao tipoOrdenacao){
        if( tipoOrdenacao == TipoOrdenacao.ASC){
            ordenarInventario();
        } else if ( tipoOrdenacao == TipoOrdenacao.DESC ){
            ordenarInventarioDesc();
        }
    }
}
