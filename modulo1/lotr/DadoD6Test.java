
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DadoD6Test {
    @Test
    public void deveRetornarEntre1e6(){
        DadoD6 dado = new DadoD6();
        int numeroAleatorio = dado.sortear();
        boolean inRange = (numeroAleatorio > 0 && numeroAleatorio < 7) ? true : false;
        
        assertTrue(inRange);
        
    }

}
