
public class DwarfBarbaLonga extends Dwarf {
    private Sorteador sorteador;
    
    public DwarfBarbaLonga(String nome) {
        super(nome);
        this.sorteador = new DadoD6();
    }
    
    // Sorteador: e passado uma instancia do dado falso com um valor falso
    public DwarfBarbaLonga( String nome, Sorteador sorteador ){
        super(nome);
        this.sorteador = sorteador;
    }
    
    @Override
    protected double calcularDano() {
        DadoD6 dado = new DadoD6();
        boolean devePerderVida = sorteador.sortear() > 2;
       
        return devePerderVida ? 10.0 : 0;
    }
    
}

