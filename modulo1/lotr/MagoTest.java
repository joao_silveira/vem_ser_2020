import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class MagoTest {
    @Test
    public void magoDeveJogarBolaDeFogoEmElfo(){
        Mago Gandalf = new Mago("Gandalf");
        Elfo Ariovaldo = new Elfo("Ariovaldo");
        
        Gandalf.jogarBolarDeFogo(Ariovaldo);
        
        assertEquals(Ariovaldo.getVida() , 80.0 , 0.1 );
    }
    
    @Test
    public void magoDeveJogarBolaDeFogoEmDwarf(){
        Mago Gandalf = new Mago("Gandalf");
        Dwarf Ariovaldo = new Dwarf("Ariovaldo");
        
        Gandalf.jogarBolarDeFogo(Ariovaldo);
        
        assertEquals(Ariovaldo.getVida() , 100.0 , 0.1 );
    }
    
    @Test
    public void magoDeveDarDanoEmArea(){
        Elfo night1 = new ElfoNoturno("Night 1");
        Elfo night2 = new ElfoNoturno("Night 2");
        Dwarf dwarf1 = new Dwarf("Dwarf1");
        Dwarf dwarf2 = new Dwarf("Dwarf2");
        Dwarf dwarf3 = new Dwarf("Dwarf3");
        Elfo night3 = new ElfoNoturno("Night 3");
        
        Mago Gandalf = new Mago("Gandalf");
       
        ArrayList<Personagem> exercito = new ArrayList<>(
            Arrays.asList(night1, night2, dwarf1, dwarf2, dwarf3, night3)
        );
        
        Gandalf.jogarBolarDeFogoEmExercito(exercito);
        
        assertEquals(exercito.get(0).getStatus(),  Status.SOFREU_DANO);
        assertEquals(exercito.get(1).getStatus(),  Status.SOFREU_DANO);
        assertEquals(exercito.get(2).getStatus(),  Status.SOFREU_DANO);
        assertEquals(exercito.get(3).getStatus(),  Status.SOFREU_DANO);
        assertEquals(exercito.get(4).getStatus(),  Status.RECEM_CRIADO);
        
    }
}
