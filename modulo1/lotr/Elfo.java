public class Elfo extends Personagem {
    private int indiceFlecha;

    private static int contadorElfos;
    
    {
        this.indiceFlecha = 1;
        this.qtdDano = 20.0;
    }
    
    public Elfo( String nome ) {
        super(nome);
        Elfo.contadorElfos++;
        this.vida = 100.0;
        this.inventario.adicionaItem(new Item(1, "Arco"));
        this.inventario.adicionaItem(new Item(2, "Flecha"));
    }
    
    public Elfo( String nome, int qtdFlecha ) {
        super(nome);
        this.inventario.adicionaItem(new Item(1, "Arco"));
        this.inventario.adicionaItem(new Item(qtdFlecha, "Flecha"));
    }
    
    public void finalize() throws Throwable {
        Elfo.contadorElfos--;
    }
    
    public void atacar(Dwarf dwarf) {
        this.atirarFlecha(dwarf);
    }
    
    public static int getQtdInstancias(){
        return Elfo.contadorElfos;
    }
    
    public Item getFlecha() {
        return this.inventario.obter(indiceFlecha);
    }
    
    public int getQtdFlechas() {
        return this.getFlecha().getQuantidade();
    }
    
    public boolean podeAtirarFlecha() {
        return this.getQtdFlechas() > 0;
    }
            
    public void atirarFlecha(Dwarf dwarf) {
        int qtdAtual = this.getQtdFlechas();
        
        if(dwarf instanceof Dwarf && podeAtirarFlecha()) {
            dwarf.sofrerDano();
            this.getFlecha().setQuantidade(qtdAtual - 1);
            //this.inventario.obter(indiceFlecha).setQuantidade(qtdAtual - 1);
            this.sofrerDano();
            aumentarXP();
        }
    }
    
    @Override
    public String imprimirNomeDaClasse(){
        return "Elfo";
    }
    
}
