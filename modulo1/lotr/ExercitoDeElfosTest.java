

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class ExercitoDeElfosTest {
    
    
    
    @Test
    public void alistarElfoVerde() {
        Elfo elfoVerde = new ElfoVerde("Green Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistarElfo(elfoVerde);
        assertTrue(exercito.getElfos().contains(elfoVerde));
    }
    
        @Test
    public void podeAlistarElfoNoturno() {
        Elfo elfoNoturno = new ElfoNoturno("Night Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistarElfo(elfoNoturno);
        assertTrue(exercito.getElfos().contains(elfoNoturno));
    }
    
    @Test
    public void buscarElfosRecemCriadosExistindo() {
        Elfo elfoNoturno = new ElfoNoturno("Night Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistarElfo(elfoNoturno);
        ArrayList<Elfo> esperado = new ArrayList<>(
            Arrays.asList(elfoNoturno)
        );
        assertEquals(esperado, exercito.filtrarElfoStatus(Status.RECEM_CRIADO));
    }
    
        @Test
    public void naoPodeAlistarElfoDaLuz() {
        Elfo elfoLuz = new ElfoDaLuz("Light Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistarElfo(elfoLuz);
        assertFalse(exercito.getElfos().contains(elfoLuz));
    }
    
}
