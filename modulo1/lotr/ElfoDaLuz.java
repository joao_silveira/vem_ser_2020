import java.util.*;

public class ElfoDaLuz extends Elfo{
    
    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(
        Arrays.asList(
            "Espada de Galvorn"
        )
    );
    
    private int ataqueContador;
    
    public ElfoDaLuz( String nome ) {
        super(nome);
        this.qtdExperienciaPorAtaque = 1;
        this.qtdDano = 21.0;
        this.qtdCura = 10.0;
        this.inventario.adicionaItem(new Item(1, "Espada de Galvorn"));
    }
    
    {
        ataqueContador = 0;
    }
    
    @Override
    public void perderItem(Item item){
        if( !item.getDescricao().equals(DESCRICOES_OBRIGATORIAS.get(0)) ){
            this.inventario.removerItem(item);
        }
    }
    
    private boolean devePerderVida(){
        return ataqueContador % 2 == 1;
    }
    
    private Item getEspada(){
        return this.getInventario().buscar(DESCRICOES_OBRIGATORIAS.get(0));
    }
    
    public void atacarComEspada(Dwarf dwarf){
        
    if( devePerderVida() ){
        this.sofrerDano();
    } else {
        this.curar();
    }
            aumentarXP();
            dwarf.sofrerDano();
    }
    
}
