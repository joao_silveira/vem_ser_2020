import java.util.*;

public class ExercitoDeElfos  {
    
    private final static ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
        Arrays.asList(
            ElfoVerde.class,
            ElfoNoturno.class
        )
    );
    
    private ArrayList<Elfo> elfosAlistados = new ArrayList<>();
    private HashMap<Status, ArrayList<Elfo>> elfosPorStatus = new HashMap<>();
    
    public void alistarElfo(Elfo elfo){
        boolean podeAlistar = TIPOS_PERMITIDOS.contains(elfo.getClass());
        
        if( podeAlistar ){
            
            elfosAlistados.add(elfo);
            
            ArrayList<Elfo> elfosDeUmStatus = elfosPorStatus.get(elfo.getStatus());
            if( elfosDeUmStatus == null ) {
                elfosDeUmStatus = new ArrayList<>();
                elfosPorStatus.put( elfo.getStatus(), elfosDeUmStatus );
            }
            elfosDeUmStatus.add(elfo);
        }
    }
    
    public ArrayList<Elfo> filtrarElfoStatus(Status status){
        return this.elfosPorStatus.get(status);
    }
    
    public ArrayList<Elfo> getElfos(){
        return this.elfosAlistados;
    }
    
}
