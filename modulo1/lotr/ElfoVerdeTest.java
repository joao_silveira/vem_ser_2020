
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest {

    @Test
    public void elfoVerdeNasceCom2Flechas() {
        ElfoVerde elfoQualquer = new ElfoVerde("legolas");
        
        assertEquals(2, elfoQualquer.getQtdFlechas());
    }
    
    @Test
    public void elfoVerdeAtiraFlechaNoDwarfGanha2Xp() {
        ElfoVerde elfoQualquer = new ElfoVerde("Rubeola");
        Dwarf dwarfQualquer = new Dwarf("Remela");
        
        elfoQualquer.atirarFlecha( dwarfQualquer );
        
        assertEquals( 100.0 , dwarfQualquer.getVida(), 0.0001 );
        
        assertEquals( 1, elfoQualquer.getQtdFlechas());
        assertEquals( 2, elfoQualquer.getExperiencia());
        
    }
    
    @Test
    public void elfoVerdeGanhaItemComDescricaoValida() {
        ElfoVerde elfoQualquer = new ElfoVerde("Rubeola");
        Item arcoDeVidro = new Item(1, "Arco de Vidro");
        
        elfoQualquer.ganharItem(arcoDeVidro);
        
        Inventario inventario = elfoQualquer.getInventario();
        
        //assertEquals(new Item(1,"Arco"), inventario.obter(0));
        //assertEquals(new Item(1,"Flecha"), inventario.obter(1));
        assertEquals(arcoDeVidro , inventario.obter(2));
    }
    
        @Test
    public void elfoVerdeGanhaItemComDescricaoInvalida() {
        ElfoVerde elfoQualquer = new ElfoVerde("Rubeola");
        Item arcoDeVidro = new Item(1, "Arco de vrido");
        elfoQualquer.ganharItem(arcoDeVidro);
        
        Inventario inventario = elfoQualquer.getInventario();
        
        assertNull(inventario.obter(2));
    }
        
        @Test
    public void elfoVerdePerdeItemComDescricaoValida() {
        ElfoVerde elfoQualquer = new ElfoVerde("Rubeola");
        Item arcoDeVidro = new Item(1, "Arco de Vidro");
        elfoQualquer.ganharItem(arcoDeVidro);
        
        Inventario inventario = elfoQualquer.getInventario();

        elfoQualquer.perderItem(arcoDeVidro);
        assertNull( inventario.buscar("Arco de Vidro"));
        
    }
    
        @Test
    public void elfoVerdePerdeItemComDecricaoInvalida(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        
        //vidro
        Item arcoDeVidro = new Item(1, "Arco de Vidro");
        
        //vrido
        Item arcoDeVrido = new Item(1, "Arco de Vrido");
        celebron.ganharItem(arcoDeVidro);
        
        Inventario inventario = celebron.getInventario();
        
        assertEquals( arcoDeVidro, inventario.obter(2) );
        
        celebron.perderItem(arcoDeVrido);
        
        assertEquals(arcoDeVidro , inventario.obter(2) );
    }
    
}
