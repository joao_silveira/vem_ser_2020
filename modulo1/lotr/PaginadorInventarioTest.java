

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PaginadorInventarioTest {

    @Test
    public void deveRetornarItensPaginados(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1, "Espada");
        Item escudo_de_metal = new Item(2, "Escudo de metal");
        Item pocao_de_HP = new Item(3, "Pocao de hp");
        Item bracelete = new Item(4, "Bracelete");
        
	inventario.adicionaItem(espada);
	inventario.adicionaItem(escudo_de_metal);
	inventario.adicionaItem(pocao_de_HP);
	inventario.adicionaItem(bracelete);
	
	PaginadorInventario paginador = new PaginadorInventario(inventario);
	paginador.pular(0);
	paginador.limitar(2);
	
	assertEquals(paginador.limitar(2).get(0).getDescricao(), "Espada");
	
	paginador.pular(2);
	
	paginador.limitar(2); // retorna os itens “Poção de HP” e “Bracelete”

	assertEquals(paginador.limitar(2).get(0).getDescricao(), "Pocao de hp");
    
    }
    
    

}
