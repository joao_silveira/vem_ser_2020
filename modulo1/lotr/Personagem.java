import java.util.*;

public abstract class Personagem {
    protected String nome;
    protected Status status;
    protected Inventario inventario;
    protected double vida;
    protected int experiencia, qtdExperienciaPorAtaque;
    protected double qtdDano;
    protected double qtdCura;
    
    {
        this.status = Status.RECEM_CRIADO;
        this.experiencia = 0;
        this.inventario = new Inventario();
        this.qtdExperienciaPorAtaque = 1;
        this.qtdDano = 0.0;
        this.qtdCura = 0.0;
    }
    
    public Personagem( String nome ) {
        this.nome = nome;
    }
    
        public String getNome() {
        return this.nome;
    }
    
    public void setNome( String nome ) {
        this.nome = nome;
    }
    
    public Status getStatus(){
        return this.status;
    }
    
    //
    public double getVida() {
        return this.vida;
    }
    
    public int getExperiencia() {
        return this.experiencia;
    }
    
    // 
    public Inventario getInventario() {
        return this.inventario;
    }
    
        public void ganharItem(Item item){
        this.inventario.adicionaItem(item);
    }
    
    public void perderItem(Item item){
        this.inventario.removerItem(item);
    }
    
    //
    protected void aumentarXP() {
        experiencia += this.qtdExperienciaPorAtaque;
    }
    
    protected double calcularDano() {
        return this.qtdDano;
    }
    
    private boolean estaVivo(){
        return this.vida > 0;
    }
    
    protected void sofrerDano() {
        this.vida -= this.vida >= calcularDano() ? calcularDano() : this.vida;
        this.status = estaVivo() ? Status.SOFREU_DANO : Status.MORTO;
    }
    
    public void curar() {
        this.vida += qtdCura;
    }
    
    public abstract String imprimirNomeDaClasse();
    
}
