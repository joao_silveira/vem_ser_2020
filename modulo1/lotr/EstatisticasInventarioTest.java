

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class EstatisticasInventarioTest {

    @Test
    public void deveRetornarAMedia(){
        Inventario inventario = new Inventario();
        
        Item arco = new Item(5,"arco");
        Item flecha = new Item(5,"flecha");
        Item escudo = new Item(5,"escudo");
        
        inventario.adicionaItem(arco);
        inventario.adicionaItem(flecha);
        inventario.adicionaItem(escudo);
        
        EstatisticasInventario inventarioEstistica = new EstatisticasInventario(inventario);
        
        assertEquals(5, inventarioEstistica.calcularMedia(), 0.01);
        
    } 
    
    @Test
    public void deveRetornarMediana(){
        Inventario inventario = new Inventario();
            
        Item arco = new Item(4,"arco");
        Item flecha = new Item(1,"flecha");
        Item escudo = new Item(5,"escudo");
            
        inventario.adicionaItem(arco);
        inventario.adicionaItem(flecha);
        inventario.adicionaItem(escudo);
            
        EstatisticasInventario inventarioEstistica = new EstatisticasInventario(inventario);
            
        assertEquals(4, inventarioEstistica.calcularMediana(), 0.01);
    }
    
    
    @Test
    public void deveRetornarQtdDeItensAcimaDaMedia(){
        Inventario inventario = new Inventario();
            
        Item arco = new Item(4,"arco");
        Item flecha = new Item(1,"flecha");
        Item escudo = new Item(5,"escudo");
            
        inventario.adicionaItem(arco);
        inventario.adicionaItem(flecha);
        inventario.adicionaItem(escudo);
            
        EstatisticasInventario inventarioEstistica = new EstatisticasInventario(inventario);
            
        assertEquals(2, inventarioEstistica.qtdItensAcimaDaMedia(), 0.01);
    
    }
    
    
    
}
