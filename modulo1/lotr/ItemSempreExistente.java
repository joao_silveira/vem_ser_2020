
public class ItemSempreExistente extends Item {
    public ItemSempreExistente( int Quantidade, String descricao ){
        super(Quantidade, descricao);
    }
    
    public void setQuantidade( int novaQuantidade ) {
        boolean podeAlterar = novaQuantidade > 0;
        
        this.quantidade = podeAlterar ? novaQuantidade : 1;
    }
}
