import java.util.*;

public class ElfoVerde extends Elfo {
    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(
        Arrays.asList(
            "Espada de aço valiriano",
            "Arco de Vidro",
            "Flecha de Vidro"
        )
    );
    
    public ElfoVerde( String nome ) {
        super(nome);
        this.qtdExperienciaPorAtaque = 2;
    }
    
    private boolean descricaoValida(Item item){
        return DESCRICOES_VALIDAS.contains(item.getDescricao());
    }
    
    @Override
    public void ganharItem(Item item){ 
        if( descricaoValida(item) ){
            this.inventario.adicionaItem(item);
        }
    }
    
    @Override
    public void perderItem(Item item){
        if( descricaoValida(item) ){
            this.inventario.removerItem(item);
        }
    }    
}
