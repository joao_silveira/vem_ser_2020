
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest {

    @Test
    public void dwarfNasceCom110DeVida() {
        Dwarf dwarf = new Dwarf("Brosteani");
        assertEquals(110.0, dwarf.getVida(), .01);
    }
    
    @Test
    public void dwarfSofre30DeDanoMudaStatus() {
        Dwarf dwarf = new Dwarf("Brosteani");
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        
        assertEquals(80.0, dwarf.getVida(), .01);
        assertEquals(Status.SOFREU_DANO, dwarf.getStatus());
    }
    
    @Test
    public void dwarfZeraVida() {
        Dwarf dwarf = new Dwarf("Broastaeni");
        
        for(int i = 0; i < 12; i++){
            dwarf.sofrerDano();
        }
        
        assertEquals(0.0, dwarf.getVida(), .01);
        assertEquals(Status.MORTO, dwarf.getStatus());
    }
    
        @Test
    public void dwarfNasceComEscudoDesequipado(){
        Dwarf dwarf = new Dwarf("Broastaeni");
        
        Item esperado = new Item(1, "Escudo");
        Item resultado = dwarf.getInventario().obter(0);
        
        assertEquals(esperado.getDescricao() , resultado.getDescricao());
    }
    
    @Test
    public void dwarfSofreDanoEquipadoComEscudo(){
        Dwarf dwarf = new Dwarf("Broastaeni");
        
        dwarf.equiparEscudo();
        dwarf.sofrerDano();
        
        assertEquals(105.0, dwarf.getVida(), .01);
        
    }

        @Test
    public void dwarfSofreDanoDesequipadoEquipaEscudoESofreDano(){
        Dwarf dwarf = new Dwarf("Broastaeni");
        
        dwarf.sofrerDano();
        
        assertEquals(100.0, dwarf.getVida(), .01);
        
        dwarf.equiparEscudo();
        dwarf.sofrerDano();
        
        assertEquals(95.0, dwarf.getVida(), .01);
        
    }
}


