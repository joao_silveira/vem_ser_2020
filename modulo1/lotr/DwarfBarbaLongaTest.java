
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfBarbaLongaTest {
    
    @Test
    public void dwarfDevePerderVida66Porcent0(){
        
        DadoFalso dadoFalso = new DadoFalso();
        dadoFalso.simularValor(3);
        DwarfBarbaLonga dwarf = new DwarfBarbaLonga("Frawd", dadoFalso);
        
        dwarf.sofrerDano();
        
        assertEquals(100.0, dwarf.getVida(), 1e-1);
    }
 
        @Test
    public void dwarfNaoDevePerderVida66Porcent0(){
        
        DadoFalso dadoFalso = new DadoFalso();
        dadoFalso.simularValor(1);
        DwarfBarbaLonga dwarf = new DwarfBarbaLonga("Frawd", dadoFalso);
        
        dwarf.sofrerDano();
        
        assertEquals(110.0, dwarf.getVida(), 1e-1);
    }
    
}
