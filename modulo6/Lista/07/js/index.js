
Array.prototype.invalidas = function() {
    let nome = "Séries Inválidas: "
    
    function temAtributoNull ( objeto ) {
        return Object.values(objeto).some( item => item == null ? true : false)
    }
    
    function anoInvalido ( objeto ) {
        return objeto.anoEstreia > new Date().getFullYear()
    }

    this.map( item => {
        if(temAtributoNull( item ) || anoInvalido(item)){
            nome = nome + item.titulo + " - "; 
        }
    })

    nome = nome.substring(0, nome.length - 3);

    return nome;
}

console.log( "Exercício 01 - " );
console.log( series.invalidas() );

Array.prototype.filtrarPorAno = function( ano ) {

    let seriesFiltradas = [];

    this.map( item => {
        if( item.anoEstreia >= ano ) {
            seriesFiltradas.push( item );
        }
    })
    return seriesFiltradas;
}

console.log("Exercício 02 - Series a partir do ano 2017");
console.log( series.filtrarPorAno(2017) ); 

Array.prototype.procurarPorNome = function( nome ) {
    let bool = false;
    
    this.map( objetoSerie => {
        objetoSerie.elenco.map( nomeElenco => {
            if(nomeElenco.includes(nome)) {
                bool = true;
            } 
        });
        
    });    

    return bool;
}

console.log("Exercício 03 - existe o nome \"João\"? : " + series.procurarPorNome("João"))

Array.prototype.mediaDeEpisodios = function() {
    let totalEpisodiosArray = 0;
    let totalDeSeries = 0;

    this.map( serie => {
        totalDeSeries++;
        totalEpisodiosArray += serie.numeroEpisodios;
    })

    return Math.trunc( totalEpisodiosArray / totalDeSeries );
}

console.log("Exercício 04 - Média de Episódios " + series.mediaDeEpisodios() )

// elenco = 40.000
// diretores = 100.000
Array.prototype.totalSalarios = function( indice ) {
    const objetoSerie = this[indice];

    let diretor = objetoSerie.diretor.length;
    let operarios = objetoSerie.elenco.length;

    return ( 40000 * operarios ) + ( 100000 * diretor );
}

console.log("Exercício 05 - Mascada em Série (0) : " + series.totalSalarios(0) );

Array.prototype.queroGenero = function ( nomeGenero ) {
    let arrayGenero = [];

    this.map( serie => {
        serie.genero.find( genero => {
            if( genero.includes(nomeGenero) ) {
                arrayGenero.push(serie);
            }
        });
    });
    return arrayGenero;
}

console.log("Exercício 06 - A - Buscas (Terror) : ");
console.log(series.queroGenero( "Terror" ));

Array.prototype.queroTitulo = function ( titulo ) {
    let arrayTitulo = [];

    this.map( serie => {
        if( serie.titulo.includes(titulo) ){
            arrayTitulo.push( serie );
        }
    });
    return arrayTitulo;
}

console.log("Exercício 06 - B - Buscas (The) : ");
console.log(series.queroTitulo( "The" ));


Array.prototype.creditos = function( indice ) {
    

    function imprimirCreditos(serie) {
        console.log(" ----------- Creditos ----------- ");
        console.log("Titulo: " + serie.titulo);
        console.log("+++++++ Diretores +++++++");
        
        serie.diretor.map( nome => console.log(nome) );

        console.log("+++++++ Elenco +++++++");
        
        serie.elenco.sort().map( nome => console.log(nome) );

    }

    imprimirCreditos(this[indice])
}

console.log("Exercício 07 - Créditos")
series.creditos(0)


Array.prototype.elencoComNomeAbreviado = function() {
    function isNomeAbreviado( nome ) {
        let bool = false;

        nome = nome.split(" ");
        let segundoNome = nome[1];
        segundoNome = segundoNome.split("");

        if( segundoNome.length == 2 && segundoNome[1] == '.' ) {
            bool = true;
        }
        return bool;
    }

    let seriesComNomeElencoAbreviado = [];
    let hashTag = "#";

    this.map( serie => {
        let todosAbreviados = true;

        serie.elenco.map( nome => todosAbreviados = isNomeAbreviado(nome));
        
        if( todosAbreviados ) {
            seriesComNomeElencoAbreviado.push(serie);
        
            serie.elenco.map( nome => hashTag = hashTag + nome.split(" ")[1].split("")[0] );
        }
    })

    //return seriesComNomeElencoAbreviado;
    return hashTag;
}

console.log("Exercício 08 - hashtag secreta");
console.log(series.elencoComNomeAbreviado());
