export default class Jogador {
    constructor(nome, numero) {
        this._nome = nome;
        this._numero = numero;
    }

    get getNome() {
        return this._nome;
    }

    get getNumero() {
        return this._numero;
    }
}