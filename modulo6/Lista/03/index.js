class Jogador {
    constructor(nome, numero) {
        this._nome = nome;
        this._numero = numero;
    }

    get getNome() {
        return this._nome;
    }

    get getNumero() {
        return this._numero;
    }

    set setNome(nome) {
        this._nome = nome;
    }

    set setNumero(numero) {
        this._numero = numero;
    }
}

class Time {
    _jogadores = [];
    _partidas = [];

    constructor(nome, tipoEsporte, status, liga) {
        this._nome = nome;
        this._tipoEsporte = tipoEsporte;
        this._status = status;
        this._liga = liga;
    }

    adicionarJogador(jogador){
        this._jogadores.push(jogador);
    }

    buscarJogadorPorNome( nome ) {
        return this._jogadores.find( jogador => jogador._nome == nome );
    }

    buscarJogadorPorNumero( numero ) {
        return this._jogadores.find( jogador => jogador._numero == numero );
    }

    buscarJogadorPorNomeENumero( nome, numero) {
        return this._jogadores.find( jogador => jogador._nome == nome && jogador._numero == numero)
    }

    findAllJogadores() {
        return this._jogadores;
    }

    get getNome() {
        return this._nome;
    }

    set setNome(nome) {
        this._nome = nome;
    }

    get getTipoEsporte() {
        return this._tipoEsporte;
    }

    set setTipoEsporte(tipo) {
        this._tipoEsporte = tipo;
    }

    get getStatus() {
        return this._status;
    }

    set setStatus(status) {
        this._status = status;
    }
    get getLiga() {
        return this._liga;
    }
    set setLiga(liga) {
        this._liga = liga;
    }
}

class Partida {
    constructor(primeiroTime, segundoTime, placarPrimeiroTime, placarSegundoTime){
        this._primeiroTime = primeiroTime;
        this._segundoTime = segundoTime;
        this._placarPrimeiroTime = placarPrimeiroTime;
        this._placarSegundoTime = placarSegundoTime;
    }

    mostrarPlacar() {
        console.log("++++++ PLACAR ++++++");
        console.log(this._primeiroTime.getNome + " : " + this._placarPrimeiroTime);
        console.log(this._segundoTime.getNome + " : " + this._placarSegundoTime);
    }
    
    get nomePrimeiroTime() {
        return this._primeiroTime.getNome;
    }

    get nomeSegundoTime() {
        return this._segundoTime.getNome;
    }
}

class Liga {
    constructor(nome){
        this._nome = nome;
    }

    _historico = [];
    
    buscarHistoricoDeTimeEmLiga(time){
        return this._historico.filter( partida => time.getNome == partida.primeiroTime.getNome || time == partida.segundoTime.getNome );
    }

    adicionarPartidaAoHistorico(partida) {
        this._historico.push(partida);
    }

    todasPartidas(){
        return this._historico;
    }
}

const sorteadorDePontos = () => Math.trunc(Math.random() * 20);

// criar liga
let ligaLocal = new Liga("Cebolao");

// criar os jogadores
let jogador1 = new Jogador("Kobe", "23");
let jogador2 = new Jogador("Lord Farquard", "13");
let jogador3 = new Jogador("Ratinho", "33");

// criar o time
let time1 = new Time("Red Canids");

time1.setLiga = ligaLocal;
time1.getLiga;

let time2 = new Time("INTZ");

console.log("times: " + time1.getNome + " e " + time2.getNome);

// adicionar jogadores no time
time1.adicionarJogador(jogador1);
time1.adicionarJogador(jogador2);
time1.adicionarJogador(jogador3);

console.log( "time: " + time1.getNome + " procurar jogador por nome");
console.log(time1.buscarJogadorPorNome("Kobe"));

console.log( "time: " + time1.getNome + " procurar jogador por numero");
console.log( time1.buscarJogadorPorNumero("13"));

console.log( "time: " + time1.getNome + " procurar jogador por nome e numero");
console.log(time1.buscarJogadorPorNomeENumero("Ratinho", "33"));

console.log( "time: " + time1.getNome + " procurar por todos jogadores");
console.log(time1.findAllJogadores());

// criar partida

let partida = new Partida(time1, time2, sorteadorDePontos(), sorteadorDePontos());
let partida2 = new Partida(time1, time2, sorteadorDePontos(), sorteadorDePontos());

partida.mostrarPlacar();
partida2.mostrarPlacar();

// adicionar partida ao historico da liga
ligaLocal.adicionarPartidaAoHistorico(partida);
ligaLocal.adicionarPartidaAoHistorico(partida2);

console.log("Buscar todas partidas de liga");
console.table(ligaLocal.todasPartidas());
