function cardapioIFood( isVeggie, isComLactose) {
    var cardapio = [
      'enroladinho de salsicha',
      'cuca de uva'
    ];

    if ( isComLactose ) {
        cardapio.push( 'pastel de queijo' );
    }

    cardapio = cardapio.concat( [
      'pastel de carne',
      'empada de legumes marabijosa'
    ] );
  
    if ( isVeggie ) {
      cardapio.splice( cardapio.indexOf( 'enroladinho de salsicha' ), 1 );
      cardapio.splice( cardapio.indexOf( 'pastel de carne' ), 1 );
    }  
    return cardapio.map( nomeDoPedido => nomeDoPedido.toUpperCase() );

  }
  
  console.log(cardapioIFood(true, true)); // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]