console.log("Exercício - 01");

/* 
Crie uma função "calcularCirculo" 
que receba um objeto com os seguintes parâmetros:
{
    		raio, // raio da circunferência
            tipoCalculo // se for "A" cálcula a área,
             se for "C" calcula a circunferência
	}
*/

const circulo1 = {
    raio: 4,
    tipoCalculo: "A"
}

const circulo2 = {
    raio: 5,
    tipoCalculo: "C"
}

const circulo3 = {
    raio: 5,
    tipoCalculo: "T"
}

function calcularCirculo( {raio, tipoCalculo} ) {
    if(tipoCalculo == "A") {
        return Math.PI * (raio ** 2);
    } else if (tipoCalculo == "C") {
        return 2 * Math.PI * raio;
    }
    return; 
}

console.log("Area do círculo: " + calcularCirculo(circulo1));
console.log("circunferência: " + calcularCirculo(circulo2));
console.log("circulo.tipoCalculo invalido: " + calcularCirculo(circulo3));

console.log("Exercício - 02");
/*
Crie uma função naoBissexto que recebe um ano (número) 
e verifica se ele não é bissexto. Exemplo:

	naoBissexto(2016) // false
	naoBissexto(2017) // true
*/

function naoBissexto(ano) {
    let multiploDe4 = ano % 4 == 0 ? true : false;

    let naoMultiploDe100 = ano % 100 != 0 ? true : false;
    
    let multiploDe400 = ano % 400 == 0 ? true : false;

    let isBissexto = (multiploDe4 && naoMultiploDe100) || multiploDe400;

    return isBissexto == true ? false : true;
}

console.log("Nao bissexto(2016): "+naoBissexto(2016));
console.log("Nao bissexto(2017): "+naoBissexto(2017));
console.log("Nao bissexto(2018): "+naoBissexto(2018));
console.log("Nao bissexto(2000): "+naoBissexto(2000));

console.log("Exercício - 03");

/*
Crie uma função somarPares que recebe um array de números (não precisa fazer validação) 
e soma todos números nas posições pares do array, exemplo:

somarPares( [ 1, 56, 4.34, 6, -2 ] ) // 3.34

*/

let arrayTeste = [1,2,8,4];

function somarPares( array ){
    let somatoria = 0;

    for(let i = 0 ; i < array.length ; i ++) {
        let isPar = i % 2 == 0 ? true : false;
        
        if(isPar) {
            somatoria += array[i];
        }
    }
    return somatoria;
}

console.log(somarPares(arrayTeste));

console.log("Exercício - 04");

/*
Escreva uma função adicionar que permite somar dois números
 através de duas chamadas diferentes (não necessariamente pra mesma função). Pirou? Ex:

adicionar(3)(4) // 7
adicionar(5642)(8749) // 14391
*/

function adicionar(numero1) {
    return function(numero2) {
        return numero1 + numero2;
    };
};

console.log(adicionar(3)(4));
console.log(adicionar(5642)(8749));


console.log("Exercício - 04");

/*
Escreva uma função imprimirBRL
 que recebe um número flutuante (ex: 4.651) e retorne como saída uma string no seguinte formato (seguindo o exemplo): “R$ 4,66”

Outros exemplos:

imprimirBRL(0) // “R$ 0,00”
imprimirBRL(3498.99) // “R$ 3.498,99”
imprimirBRL(-3498.99) // “-R$ 3.498,99”
imprimirBRL(2313477.0135) // “R$ 2.313.477,02”

OBS: note que o arredondamento sempre deve ser feito para cima, em caso de estourar a precisão de dois números
OBS: o separador decimal, no Brasil, é a vírgula
OBS: o separador milhar, no Brasil, é o ponto

*/

function imprimirBRL( valorFlutuante ){

}


/* NAO CONSIDERAR - TESTES
var number = 6.688689;
console.log(number);
var roundedNumber = Math.ceil(number * 100) / 100;
console.log(roundedNumber);

var roundedToString = roundedNumber.toString();

console.log(roundedToString);

console.log("Outro teste");

var number2 = 1234.88988;
console.log(number2);

var roundedNumber2 = Math.ceil(number2 * 100) / 100;

console.log(roundedNumber2);

var roundedToString2 = (roundedNumber2 / 1000) % 10;

console.log(roundedToString2);
*/
