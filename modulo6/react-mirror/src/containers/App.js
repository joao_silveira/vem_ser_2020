import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import Home from './home';
import Lista from './avaliacoes';
import DetalhesEpisodios from './detalhesEpisodios';
import TodosEpisodios from './todosEpisodios';

export default class App extends Component {
  
  render(){
    
    return ( 
      <Router>
        <Route path="/" exact component={ Home } />
        <Route path="/avaliacoes" exact component={ Lista } />
        <Route path="/episodio/:id" exact component={ DetalhesEpisodios } />
        <Route path="/episodios" exact component={ TodosEpisodios } />
      </Router>
      )
  };
}

const paginaTeste = () => 
  <div>
    Pagina teste
    <h1>Essa pagina e secundaria</h1>
    <Link to="/"> home</Link>
  </div>
