
/** -----------------VAR E LET------------------------ */

let nome1 = "tal coisa 1";
nome1 = "tal coisa 12222";

{
    let nome1 = "Outra coisa";
}

const cpf = "00000000000";
const doubleV = 1.3;

const pessoa = {
    nome: "jv"
};

Object.freeze(pessoa);

const pessoaModificada = pessoa;
pessoaModificada.nome = "Marcos1";
pessoaModificada.idade = 30;
pessoaModificada.status = "Um guri!";

/** --------------------FUNCTION----------------------- */

let nome = "Marcos";
let idade = 30;
let semestre = 5;
let notas = [10, 9 ,9.5, 9.7];

let nome2 = "Marcos H";
let idade2 = 30;
let semestre2 = 7;
let notas2 = [8, 7 ,7.1, 7.6];

//Factory
function funcaoCriacaoObjetoAluno(nomeExt, idadeExt, semestreExt, notas = []) {
    const aluno = {
        nome: nomeExt,
        idade: idadeExt,
        semestre: semestreExt
    }

    function verificarAprovacao(notas) {
        if(notas.length == 0){
            return "Sem Notas";
        }

        let somatoria = 0;
        for (let i = 0; i < notas.length; i++) {
            somatoria += notas[i];
        }
        return somatoria / notas.length > 7 ? "Aprovado" : "Reprovado";
    }

    aluno.status = verificarAprovacao(notas);
    
    return aluno;
}

console.log(funcaoCriacaoObjetoAluno(nome, idade, semestre, notas));

/** --------------------Template String----------------------- */

function funcaoTesteConcatETemplate(teste = [], testeComObjetos = "") {

    function testandoTemplate(teste) {
        let valor1 = teste[0];
        let valor2 = teste[1];
        console.log( `A quantidade de dia é ${valor1 + valor2}` );
        
        let texto = "Varias frutas:" +
                    "\n" +
                    "- Banana" +
                    "\n" +
                    "- Ameixa" +
                    "\n" +
                    "- Uva" +
                    "\n" +
                    "- Caqui";

        let texto1 = `Varias frutas:
            - Banana
            - Ameixa
            - Uva
            - Caqui`;

        console.log(texto1);

    }

    function testandoTemplateComObjeto(teste) {
        if (teste === "") {
            return;
        }

        console.log( `Meu nome é ${ teste.nome } tenho ${ teste.idade } e sou ${ teste.nacionalidade }.` );
    }

    testandoTemplate(teste);
    testandoTemplateComObjeto(testeComObjetos);
}

const teste = [30, 5];
const testeObj = {
    nome: "Marcos",
    idade: 30,
    nacionalidade: "brasileiro"
}

/** --------------------Desconstruction----------------------- */

function funcaoTesteDestruction(testeComObjetos) {
    function testando({ nome, idade, nacionalidade }) {
        console.log( `Meu nome é ${ nome } tenho ${ idade } e sou ${ nacionalidade }.` );
    }

    testando(testeComObjetos);
}

const testeObj1 = {
    nome: "Marcos",
    idade: 30,
    nacionalidade: "brasileiro"
}

/** --------------------Spread Operator----------------------- */

function funcaoTesteSpread( ...obj ) {
    
    for (let i = 0; i < obj.length; i++) {
        console.log(obj[i]);
    }
}

funcaoTesteSpread("Marcos", "R$50,00", "Qualquer1");