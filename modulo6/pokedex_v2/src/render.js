
const renderizar = ( pokemon ) => { // eslint-disable-line no-unused-vars
  const input = document.querySelector( 'input' );
  input.value = pokemon.id;

  const nome = document.querySelector( '.name' );
  nome.innerHTML = `#${ pokemon.id } ${ pokemon.nome }`;

  const imgPokemon = document.querySelector( '.pokeImage' );
  imgPokemon.style.display = 'block';
  imgPokemon.src = pokemon.imagemFrente;

  const altura = document.querySelector( '.heightText' );
  altura.innerHTML = pokemon.altura;

  const peso = document.querySelector( '.weightText' );
  peso.innerHTML = pokemon.peso;

  const tipo = document.querySelector( '.typeText' );
  tipo.innerHTML = pokemon.tipo;

  const stats = document.querySelector( '.statsString' );
  const statsData = pokemon.estatistica.map( item => `${ item[0] }:${ item[1] }` ).join( '\n' );
  console.log( statsData );

  stats.innerHTML = statsData;

  const rotateButton = document.querySelector( '.select' );


  function rotate() {
    if ( pokemon.imagemCosta == null ) return;

    if ( imgPokemon.src === pokemon.imagemFrente ) {
      imgPokemon.src = pokemon.imagemCosta
    } else {
      imgPokemon.src = pokemon.imagemFrente;
    }
  }

  rotateButton.onclick = rotate;
}
