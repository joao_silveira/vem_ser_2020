class Pokemon { // eslint-disable-line no-unused-vars
  constructor( objDaAPI ) {
    this._nome = objDaAPI.name;
    this._id = objDaAPI.id;
    this._imagem_frente = objDaAPI.sprites.front_default;
    this._imagem_costas = objDaAPI.sprites.back_default;
    this._altura = objDaAPI.height;
    this._peso = objDaAPI.weight;
    this._tipo = objDaAPI.types;
    this._estatisticas = objDaAPI.stats;
  }

  get nome() {
    return this._nome;
  }

  get id() {
    return this._id;
  }

  get imagemFrente() {
    return this._imagem_frente;
  }

  get imagemCosta() {
    return this._imagem_costas
  }

  get altura() {
    return `${ this._altura * 10 } cm`;
  }

  get peso() {
    return `${ this._peso / 10 } kg`;
  }

  get tipo() {
    return this._tipo.map( item => item.type.name );
  }

  get estatistica() {
    return this._estatisticas.map( item => [item.stat.name, item.base_stat] );
  }
}
