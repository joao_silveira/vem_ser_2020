/* eslint-disable no-undef */
const buscarPokemon = ( valor, api ) => { // eslint-disable-line no-unused-vars
  if ( !( valor > 0 && valor <= 893 ) ) {
    alert( 'Digite um id válido' );
    return;
  }

  if ( valor !== localStorage.getItem( 'atual' ) ) {
    api
      .buscarEspecifico( valor )
      .then( pokemon => {
        const poke = new Pokemon( pokemon );

        localStorage.clear( 'atual' );
        localStorage.setItem( 'atual', pokemon.id );

        renderizar( poke );
      } );
  }
}
