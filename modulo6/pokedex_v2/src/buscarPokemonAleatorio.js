/* eslint-disable no-undef */
const buscarAleatorio = ( api ) => { // eslint-disable-line no-unused-vars
  // eslint-disable-next-line no-use-before-define
  const numeroAleatorio = gerarNumeroAleatorioInexistente();

  console.log( numeroAleatorio );

  api
    .buscarEspecifico( numeroAleatorio )
    .then( pokemon => {
      localStorage.clear( 'atual' );
      localStorage.setItem( 'atual', pokemon.id );

      const poke = new Pokemon( pokemon );

      renderizar( poke );
    } );
}

function existeNoArray( numero ) {
  const arrayNumerosJaSorteados = localStorage.getItem( 'numerosJaGerados' );

  // eslint-disable-next-line no-unused-vars
  let existe = false;


  if ( arrayNumerosJaSorteados === null ) {
    // eslint-disable-next-line prefer-const
    let arrayNovo = 0;

    localStorage.setItem( 'numerosJaGerados', arrayNovo );

    return existe;
  }

  for ( let i = 0; i < arrayNumerosJaSorteados.length; i + 1 ) {
    if ( numero === arrayNumerosJaSorteados[i] ) {
      existe = true;
      break;
    }
  }
  return existe;
}

function gerarNumeroAleatorioInexistente() {
  let numeroAleatorio = Math.trunc( Math.random() * 893 + 1 );

  while ( existeNoArray( numeroAleatorio ) ) {
    numeroAleatorio = Math.trunc( Math.random() * 893 + 1 );
  }

  // eslint-disable-next-line prefer-const
  const numerosJaGerados = localStorage.getItem( 'numerosJaGerados' );

  const numerosJageradosNovo = numerosJaGerados.split().push( numeroAleatorio );

  localStorage.setItem( 'numerosJaGerados', numerosJageradosNovo );

  return numeroAleatorio;
}
