/* eslint-disable no-undef */
const pokeApi = new PokeApi();

// config - limpa o localStorage
// window.onload = window.localStorage.clear();

const input = document.querySelector( 'input' );
const estouComSorte = document.querySelector( '.blueButton' );

input.addEventListener( 'blur', () => buscarPokemon( input.value, pokeApi ) );

estouComSorte.addEventListener( 'click', () => buscarAleatorio( pokeApi ) );
