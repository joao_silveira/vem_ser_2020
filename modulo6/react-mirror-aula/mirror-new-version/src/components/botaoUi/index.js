import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { Button } from 'antd';

import "./botaoUi.scss";

const BotaoUi = ( { classe, metodo, nome, link } ) =>
  <React.Fragment>
    <Button className={`btn ${ classe }`} onClick={ metodo }>
      { link ? <Link to={ link }>{ nome }</Link> : nome }
    </Button>
  </React.Fragment>

BotaoUi.propTypes = {
  nome: PropTypes.string.isRequired,
  metodo: PropTypes.func,
  classe: PropTypes.string
}

export default BotaoUi;