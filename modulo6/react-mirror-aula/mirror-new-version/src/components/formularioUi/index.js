import React, { useState, useEffect } from 'react';
import EpisodiosApi from '../../models/episodiosApi';

function Formulario(props) {
  const [ qtdAssistida, setQtdAssistida ] = useState(1);
  const [ nome, setNome ] = useState('Marcos');

  useEffect( () => {
    /* this.episodioApi = new EpisodiosApi();
    this.episodioApi.registrarNota({ nota: 10, episodioId: 14 }); */
  }, [nome]);

  return (
    <React.Fragment>
      <p>Implementação de formulário:</p>
      <p>{ qtdAssistida }</p>
      <button type="button" onClick={ () => setQtdAssistida( qtdAssistida + 1 ) }>Já assisti!</button>
      <input type="text" onBlur={ evt => setNome( evt.target.value ) } />
      <p>{ nome }</p>
    </React.Fragment>
  )
}

export default Formulario;