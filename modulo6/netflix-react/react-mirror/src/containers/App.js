import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Home from './home';
import DetalhesEpisodios from './detalhesEpisodios';
import RankingEpisodios from './rankingEpisodios';

export default class App extends Component {  
  render(){
    return ( 
      <Router>
        <Route path="/" exact component={ Home } />
        <Route path="/episodio/:id" exact component={ DetalhesEpisodios } />
        <Route path="/ranking" exact component={ RankingEpisodios } />
      </Router>
      )
  };
}
