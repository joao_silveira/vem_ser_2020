import React, { Component } from 'react';
import { EpisodiosApi, ListaEpisodios } from '../../models';
import { HeaderUi, RankingUi } from '../../components';
import { Layout, Button } from 'antd';

const { Footer, Content } = Layout;

export default class RankingEpisodios extends Component {
  constructor( props ) {
    super( props );
    this.episodiosApi = new EpisodiosApi();
    this.state = {
      listaEpisodios: []
    }
  }

  componentDidMount() {
    const requisicoes = [
      this.episodiosApi.buscar(),
      this.episodiosApi.buscarTodasNotas(),
      this.episodiosApi.buscarTodosDetalhes()
    ]

    Promise.all( requisicoes )
    .then( respostas => {
			const listaEpisodiosAux = new ListaEpisodios( respostas[ 0 ], respostas[ 1 ], respostas[ 2 ] );
			
      this.setState( state => { return { ...state, listaEpisodios: listaEpisodiosAux._todos, notasEpisodios: respostas[1] } } );
    } )
  }

  ranking = () => {
    const { listaEpisodios, notasEpisodios } = this.state;
		const notas = [];
		
    listaEpisodios.forEach( item => {
      let total = 0;
      let qtdNotas = 0;
      notasEpisodios.forEach( n => {
        if( n.episodioId === item.id ) {
          total += n.nota;
          qtdNotas++;
        }
      });
      const media = total / qtdNotas;
      notas.push( { id: item.id, media: ( !media ? 0 : media  ) } )
    });

    notas.sort( ( a, b ) => {
      return b.media - a.media;
    })

    let episodios = [];
    notas.forEach( nota => {
      listaEpisodios.forEach( episodio => {
        if( episodio.id == nota.id ) {
          episodio.media = nota.media;
          episodios.push( episodio );
        }
      });
    });
    return episodios;
  }

  render() {
    
    const lista = this.ranking();

    return (
      <>
      <Layout>
          <HeaderUi> </HeaderUi>
          <Content className="back-ground-black " >
            <RankingUi listaRankeados={lista} ></RankingUi>     
          </Content>
      </Layout>
      </>
    )
  }
}