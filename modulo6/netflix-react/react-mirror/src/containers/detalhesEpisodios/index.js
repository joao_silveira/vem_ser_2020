import React, { Component } from 'react';
import { EpisodiosApi, Episodio } from '../../models';
import { Rate, Layout } from 'antd';
import { HeaderUi, EpisodioUi } from '../../components';

import './index.scss';

const { Footer, Content } = Layout;

export default class DetalhesEpisodios extends Component {
	constructor( props ) {
		super( props );
		this.episodioApi = new EpisodiosApi();
		this.state = {
			detalhes: null
		}
	}

	componentDidMount() {
		const episodioId = this.props.location.state;
		const requisicoes = [
      this.episodioApi.buscarEpisodio( episodioId ),
			this.episodioApi.buscarDetalhes( episodioId ),
			this.episodioApi.buscarNota( episodioId )
		];

		Promise.all( requisicoes )
			.then( respostas => {
        const { id, nome, duracao, temporada, ordemEpisodio, thumbUrl } = respostas[0];
				this.setState({
          episodio: new Episodio( id, nome, duracao, temporada, ordemEpisodio, thumbUrl ),
					detalhes: respostas[1],
					objNota: respostas[2]
				})
			})
  }
  
  registrarNota( nota ) {
    const { episodio } = this.state;
    
    if (episodio.validarNota( nota )) {
      episodio.avaliar( nota );
    }

    this.setState((state) => {
      return { ...state, episodio };
    });
    this.episodioApi.buscarNota( episodio.id )
      .then( resposta => {
        this.setState( {
          objNota: resposta
        })
      })
  }

  mediaNotaEpisodio = ( objNota ) => {
    const tamanho = objNota.length;
    let total = 0;
    
    if( tamanho === 0 ) {
      return total;
    }
    
    objNota.map( item => {
      total += item.nota;
    });
    return total / tamanho;
  }

	render() {
		const { episodio, detalhes, objNota } = this.state;
    
    if( objNota !== undefined ) {
      console.log(objNota.lenght);
    }

    return (
			<>
        <Layout>
          <HeaderUi></HeaderUi>
          <Content className="black-ground" >

            { episodio && (<EpisodioUi episodio={ episodio } /> )}
            <div className="container">
            <div className="row">
            {
              detalhes ?
              <div>
                <p> { detalhes.sinopse } </p>
                <span>{ new Date( detalhes.dataEstreia ).toLocaleDateString() }</span>
                <span>IMDb: { detalhes.notaImdb * 0.5 }</span>      
                <Rate 
                  defaultValue={ this.mediaNotaEpisodio( objNota ) } 
                  onChange={ (eventValue) => this.registrarNota(eventValue)}
                />
              </div> : null
            }
            </div>
            </div>
          </Content>
				</Layout>
			</>
		)
	}
}