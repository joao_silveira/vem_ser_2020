import React, { Component } from 'react';
import { ListaEpisodios, EpisodiosApi } from '../../models';
import { Layout, Button } from 'antd';
import { ListaEpisodiosHorizontal, HeaderUi } from '../../components';

import './Home.scss';

const { Footer, Content } = Layout;

export default class Home extends Component {  
  constructor(props) {
    super(props);
    this.episodiosApi = new EpisodiosApi();
    
    this.state = {
      listaEpisodios: []
    }
  }

  componentDidMount() {
    const requisicoes = [
      this.episodiosApi.buscar(),
      this.episodiosApi.buscarTodasNotas(),
      this.episodiosApi.buscarTodosDetalhes()
    ];
    Promise.all( requisicoes )
      .then( respostas => {
        const listaEpisodios = new ListaEpisodios( respostas[0], respostas[1], respostas[2] );
        this.setState( state => {
          return { ...state, listaEpisodios: listaEpisodios._todos }
        });
      });
  }

  buscaTermo = (value) => {
    const termo = value;

    this.episodiosApi.filtrarPorTermo(termo).then((resultados) => {
      let listaFiltro = [];
      
      resultados.forEach((item) => {
        this.state.listaEpisodios.forEach((ep) => {
          if (item.id === ep.id) {
            listaFiltro.push(ep);
          }
        });
      });
      
      this.setState((state) => {
        return { ...state, listaEpisodios: listaFiltro };
      });
    });
  };

  render(){
    const { listaEpisodios } = this.state;

    return(
      <>
        <Layout>
          <HeaderUi filtrarPorTermo={ this.buscaTermo.bind(this) } > </HeaderUi>
          <Content className="back-ground-black " >
            <ListaEpisodiosHorizontal listaEpisodios = { listaEpisodios }>
            </ListaEpisodiosHorizontal>
          </Content>
        </Layout>
      </>
    )
  };
}