import React, { Component } from 'react';
import { Input, Button } from 'antd';
import xilften_logo from '../../assets/xilften_logo.png';
import { Link } from 'react-router-dom';
import './index.css';

const { Search } = Input;

const onSearch = value => console.log(value);

class HeaderUi extends Component {
  constructor(props){
    super(props);
    console.log(props);
  }

  render() {
    let sortear = Math.floor(Math.random() * 19) + 1;
    let {filtrarPorTermo} = this.props; 
    return (
      <header className="Header">
        <Link to="/">
          <img src={xilften_logo} className="logo" />
        </Link>

        <Search placeholder="Pesquise por um episódio" onSearch={filtrarPorTermo} enterButton className="search" />

        <div>
          <Link to={{
            pathname: `/episodio/${ sortear }`,
            state: sortear
          }}>
            <Button type="primary" >
              Aleatório
            </Button>
          </Link>
          
          <Link to={{
            pathname: `/ranking`,
            state: sortear
          }}>
            <Button type="primary" className="margin-left">
              Ranking
            </Button>
          </Link>
        </div>
      </header>
    );
  }
}

export default HeaderUi;