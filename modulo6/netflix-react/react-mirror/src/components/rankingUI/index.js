import React from 'react';
import { Rate } from 'antd';
import episodio from '../../models/episodio'

import './index.scss';

const RankingUi = (props) => {
	console.log(props.listaRankeados)
	return (
		<>
			<div>
				
				{ props.listaRankeados.map( item => {
					return (
						<div className='card' key={ item.id }>
							<img src={item.url} alt={ `Episódio ${item.nome}` } className=""/>
							<span>{item.nome} - </span>
							<Rate disabled defaultValue={item.media} />
            </div>
					)
				}) }
			</div>
		</>
	);
};

export default RankingUi;