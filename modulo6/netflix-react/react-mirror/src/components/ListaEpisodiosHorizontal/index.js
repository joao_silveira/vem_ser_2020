import React, { Component } from 'react';
import { Card, Rate, Col, Row } from 'antd';
import { Link } from 'react-router-dom';
import './index.scss';

const { Meta } = Card;

const ListaEpisodiosHorizontal = ( { listaEpisodios } ) => {
  console.log(listaEpisodios[0])

	return(
		<>
      <div className="container">
        { listaEpisodios && 
          listaEpisodios.map( episodio => {
            return(
              <Card
                key={ episodio.id }
                className="white-text card-gray espaco-entre-cards"
                hoverable
                bordered={false}
                style={{  width: 300,height: 410 }}
                cover={<img alt="example" src={episodio.url} />}
              >
              <Link to={{
                pathname: `/episodio/${ episodio.id }`,
                state: episodio.id
              }}>
                <h4>{episodio.nome}</h4>
              </Link>
              
                <Rate disabled defaultValue={episodio.nota} />
                <p>Duração: {episodio.duracao} minutos</p>
                <p>{episodio.sinopse}</p>
              </Card>
              );
          })
        }
      </div>
		</>
	)
}

export default ListaEpisodiosHorizontal;