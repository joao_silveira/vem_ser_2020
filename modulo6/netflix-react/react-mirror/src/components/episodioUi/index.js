import React, { Component } from 'react';
import './index.scss';
import './grid.scss';

export default class EpisodioUi extends Component {
  render(){
    const { episodio } = this.props;
    
    return (
      <React.Fragment>

        <div className="container" >
      
            <img src={ episodio.url } alt={ episodio.nome } className="img"/>
          
        </div>
        <div className="container" >
          <div className="row">
            <h2>{ episodio.nome }</h2>
            <p>Duração: { episodio.duracaoEmMin }</p>
            <p>Temporada / Episódio: { episodio.temporadaEpisodio }</p>
            <p>Nota: { episodio.nota }</p>
          </div>  
        </div>
      </React.Fragment>
    )
  }
}