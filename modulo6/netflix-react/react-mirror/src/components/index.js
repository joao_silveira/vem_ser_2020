import EpisodioUi from './episodioUi';
import HeaderUi from './headerUi';
import ListaEpisodiosHorizontal from './ListaEpisodiosHorizontal';
import RankingUi from './rankingUI'

export { RankingUi, ListaEpisodiosHorizontal, EpisodioUi, HeaderUi};
