import React, { useEffect, useState } from 'react';

import { Button } from 'antd';
import { Link, useHistory } from 'react-router-dom';
import { Divider, Checkbox } from 'antd';

import './index.scss';
import EnjoeiApi from '../../models/enjoeiApi';

function LoginUi() {
  const history = useHistory();
  
  const [ login, setLogin ] = useState();
  const [ senha, setSenha ] = useState();
  const [ logar, setLogar ] = useState();
  
  const enjoeiApi = new EnjoeiApi();

  function verifyAndRedirect() {
    if(localStorage.getItem("user")){
      history.push('/')
    }
  }

  return (
    <>
      <div className="container-login">
        <h2 className="">faça login no enjoei</h2>
        <div className="" >
          
          <Button className="btn button-facebook" >Entre usando o facebook</Button>

          <Divider plain>ou</Divider>

          <div className="input-group">
            <label className="label">e-mail</label>
            
            <input onBlur={ e => setLogin( e.target.value )} id="email" type="text" name="email"/>
            
            <label className="label">senha</label>
            
              <input onBlur={ e => setSenha( e.target.value )} type="password" id="senha" name="senha"/>
            
          </div>

          <Checkbox className="align-left">continuar conectado</Checkbox>
          
          <Link className="align-right">esqueci a senha</Link>
          
          <button className="btn button-login" onClick={ verifyAndRedirect() } >Entrar</button>
          
        </div>

        <Link to="/register" className="">não tenho conta</Link>
      
      </div>
    </>
  )
}

export default LoginUi;
