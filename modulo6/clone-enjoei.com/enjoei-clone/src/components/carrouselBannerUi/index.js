import React, { useEffect, useState } from 'react';
import { Carousel } from 'antd';
import './index.scss';
import EnjoeiApi from '../../models/enjoeiApi';

function CarrouselBannerUi() {
  
  const [ banner, setBanner ] = useState([]);

  useEffect( () => {
    const enjoeiApi = new EnjoeiApi();

    enjoeiApi.buscarBanners().then( res => setBanner(res) );
  }, [] );

    return (
      <>
        <Carousel autoplay className="banner">
          { banner &&
            banner.map( item => {
              return (
                <div>
                  <img src={item.imagem} />  
                </div>
              )
            } )
          }
        </Carousel>
      </>
    );
}

export default CarrouselBannerUi;