import React from 'react';
import { PageHeader, Button, Input, Avatar } from "antd";
import { QuestionOutlined, SearchOutlined, ArrowLeftOutlined } from '@ant-design/icons';
import { Link } from "react-router-dom";
import MensagemHeader from '../mensagemHeader';
import './index.scss';

import logo_enjoei from '../../assets/logo_enjoei.png';

const HeaderUi = (props) => {
  return (
    <>
    { props.isRegisterPage !== true ? (
      
      <div className="header-wrap">
        <MensagemHeader mensagem="voa, frete, voa 🐥 só até R$ 4,99 - por tempo limitado nesse baaando de produtos" />
        <PageHeader
        className="page-header"
        title={<Link to="/"><Avatar size={35} src={logo_enjoei} /> </Link>}
        subTitle={
          <Input placeholder={`Busque por "corello"`} className="input-header" suffix={<SearchOutlined />}/>
        }
        extra={[
        <button key="1" className="button-header-1" >
          moças
        </button>,
        <button key="2" className="button-header-1" >
          rapazes
        </button>,
        <button key="3" className="button-header-1" >
          kids
        </button>,
          <button key="4" className="button-header-1" >
            casa&tal
          </button>,
          
          <div className="separator">
          </div>,

          <QuestionOutlined />,
          
          
            <button key="5" className="button-header-1">
              <Link to="/login" > <span style={{ color: "black" }}>entrar</span> </Link>
            </button>
          ,

          <button key="6" className="button-header-2" text>
            quero vender
          </button>
        ]}
        />
      </div> 
      
    ) : (
      
      <div className="header-wrap">
        <PageHeader className="page-header">

        <Link to="/"><Avatar size={35} src={logo_enjoei} className="avatar-header-register" /></Link>

        </PageHeader>
      </div>
      
    ) }
    </>
  );
};

export default HeaderUi;
