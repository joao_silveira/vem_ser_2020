import { Button } from 'antd';
import React, { useEffect, useState } from 'react';
import './index.scss';

import { Link, useHistory } from 'react-router-dom';
import { Divider, Checkbox } from 'antd';
import EnjoeiApi from '../../models/enjoeiApi';

const RegisterUi = () => {
  const enjoeiApi = new EnjoeiApi();

  const history = useHistory();

  const [ nome, setNome ] = useState();
  const [ email, setEmail ] = useState();
  const [ senha, setSenha ] = useState();
  const [ salvar, setSalvar ] = useState();

  useEffect( () => {
    enjoeiApi.salvaUsuario( nome, email, senha );
    localStorage.setItem('user', [nome, email, senha] );
    history.push('/');
  }, [salvar] );

  return (
    <>
      <section className="container-login">
        <h2 className="">crie sua conta no enjoei</h2>
        <div>
          <div className="input-group">

            <label className="label">Nome completinho</label>
            <input  id="nome" type="text" name="nome" onBlur={ (event) => setNome(event.target.value) }/>

            <label className="label">e-mail</label>
            <input  id="email" type="text" name="email" onBlur={ (event) => setEmail(event.target.value) } />
            
            <label className="label">senha super secreta</label>
            
            <input  type="password" id="senha" name="senha" onBlur={ (event) => setSenha(event.target.value) }/>
          </div>
          <div>
            <button className="" onClick={ evt => setSalvar( true )} >Criar conta</button>
          </div>
        </div>
        <Divider plain></Divider>
        <Button className="btn button-facebook" >Entre usando o facebook</Button>
      </section>
    </>
  )
};

export default RegisterUi;

