import React, { useEffect, useState } from 'react';
import { Input } from 'antd';
import EnjoeiApi from '../../models/enjoeiApi';

import './index.scss';

const { Search } = Input;

const CommentSection = (props) => {
  const enjoei = new EnjoeiApi();
  const [comentarios, setComentarios] = useState([]);

  function registraComentarioAtualizar(comentario) {
    const reqs = [
      enjoei.registrarComentario( props.produtoId, comentario, 'voce' ),
      enjoei.buscarComentario( props.produtoId)
    ];

    Promise.all( reqs )
      .then( res => {
        setComentarios(res[1]);
    })
  }

  useEffect( () => {
    enjoei.buscarComentario( props.produtoId ).then( res => {
      setComentarios(res);
    })
  }, [] );
  
  return (
    <>
      <div className="comment-section-cp">
        <Search
            placeholder="Deixe seu comentário"
            allowClear
            enterButton="Comentar"
            size="large"
            onSearch={ (eventValue) => registraComentarioAtualizar(eventValue) }
            maxlength="280"
          Search />

        {
          comentarios && comentarios.map( c => <p> {c.comentario} - {c.usuario}</p>)
        }
      </div>
    </>
  );
};

export default CommentSection;