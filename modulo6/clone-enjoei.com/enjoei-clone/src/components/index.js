import HeaderUi from './headerUi';
import CarrouselBannerUi from './carrouselBannerUi';
import ContainerProductSquare from './containerProductSquare';
import RegisterUi from './registerUi';
import LoginUi from './loginUi';
import ProductUi from './productUi';
import FooterUi from './footerUi';

export { ProductUi, LoginUi, RegisterUi, HeaderUi, FooterUi, CarrouselBannerUi, ContainerProductSquare };