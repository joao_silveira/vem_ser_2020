import React from 'react';
import './index.scss';

const MensagemHeader = ( { mensagem } ) => (
  <React.Fragment>
    <div className="mensagem-header-container">
      <p> { mensagem } </p>    
    </div>
  </React.Fragment> 
)

export default MensagemHeader;