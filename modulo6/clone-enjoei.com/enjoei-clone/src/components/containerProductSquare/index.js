import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import EnjoeiApi from '../../models/enjoeiApi';
import './index.scss';

import Square from './square';

export default class ContainerProductSquare extends Component {
  constructor(props) {
    super(props);
    this.enjoeiApi = new EnjoeiApi();
    this.state = {
      listaCategorias: [],
      listaProdutos: [],
    } 
  }

  componentDidMount() {
    let categorias = [];
    let produtos = [];

    const requisicoes = [
      this.enjoeiApi.buscarCategorias(),
      this.enjoeiApi.buscarProdutos()
    ];

    Promise.all(requisicoes).then((respostas) => {
      categorias = respostas[0];
      produtos = respostas[1];
      
      this.setState((state) => {
        return {
          ...state,
          listaCategorias: categorias,
          listaProdutos: produtos,
        };
      });
    });
  }

  gerarRandom() {
    return Math.floor((Math.random() * 3) + 1);
  }

  render() {

    const { listaCategorias, listaProdutos } = this.state;

    
  return ( 
    <>
      { listaCategorias && listaCategorias.map( categoria => {
        return( 
        <div className="container-">
          <h2> {categoria.nome} </h2>
          <div className="container-products-subtitle"> 
            <p> {categoria.subTitulo} </p>      
            <Link className="link-">ver mais > </Link> 
          </div>
          
          <div className={`container-products grid-template-areas-ct-square-${this.gerarRandom()} grid`}>
          
          {
            <Square categoria={categoria.id} produtos={listaProdutos}/>
          }
          
          </div>
        </div>
        );
        })
      }
    </>
  );

  }
};
