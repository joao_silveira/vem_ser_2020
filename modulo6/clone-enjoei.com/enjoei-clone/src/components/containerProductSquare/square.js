import React from 'react';
import { Link } from 'react-router-dom';
import { Typography } from 'antd';
import './index.scss';

const { Text } = Typography;

const Square = ( props ) => {
  const categoria = props.categoria;
  
  const produtos = props.produtos.filter( produto => {
    if(categoria === produto.idCategoria){
      return produto;
    }
  } ); 

  return (
    <>
      {produtos && produtos.map( (produto, index) => {
        return (

            <div style={{  
              backgroundImage: `url(${produto.imagem})`,
              backgroundPosition: 'center',
              backgroundSize: 'cover',
              backgroundRepeat: 'no-repeat'
            }} 
            className={`product-sm-div item-tp-area-${index + 1}`}>
              <Link className="link-display-block" 
              to={ { pathname: "/produto", state: { produtoId: produto.id, vendedorId: produto.idVendedor } } }
              >
                <div className="price"> {`R$ ${produto.preco}`} </div> 
              </Link>
            </div>
          
          )
      })}
    </>
  );
};

export default Square;
