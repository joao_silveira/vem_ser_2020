import React, { Component } from 'react';
import CommentSection from '../productUiCommentSection';
import EnjoeiApi from '../../models/enjoeiApi';
import './index.scss';

class ProductUi extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      produto: undefined,
      detalhe: undefined,
      vendedor: undefined
    }
  }

  componentDidMount() {
    const enjoeiApi = new EnjoeiApi();

    const requests = [
      enjoeiApi.buscarProduto( this.props.idProduto ),
      enjoeiApi.buscarDetalheDeProduto( this.props.idProduto ),
      enjoeiApi.buscarVendedor( this.props.idVendedor )
    ];

    Promise.all( requests )
      .then( res => {
        this.setState( (state) => {
          return {  ...state, produto: res[0], detalhe: res[1], vendedor: res[2] }
        })
      })
  }
  
  dataFormat = (data) => {
    const months = [ "jan", "fev", "mar", "abr", "maio", "jun", "jul", "ago", "set", "out", "nov", "dez" ];
    const dataArray = data.split("-");
    let month = parseInt(dataArray[1]);
    return `${months[ month ]}/${dataArray[0]}`
  }

  render() {
    const { produto, detalhe, vendedor } = this.state;

    return (
      <>
        { produto &&
        <div className="container-products-ui">
          <div className="product-image-section">
            <div style={{  
              backgroundImage: `url(${produto.imagem})`,
              backgroundPosition: 'center',
              backgroundSize: 'cover',
              backgroundRepeat: 'no-repeat',
              height: '550px'
            }} > </div>
            
            {/* componentizar commentSection */}
            <div className="comment-section">
              
              <CommentSection produtoId = {produto.id} />
            
            </div>

          </div>

          <div className="column-separator">
            <div className="icon-shape" ></div>
            <div className="icon-shape" ></div>
          </div>
          

          <div className="product-info">
            <span className="category">moças/roupas/vestidos</span>

            <h2 className="product-name">{produto.nome}</h2>
            <p className="product-brand">{detalhe.marca} <span style={{ color: "lightgray"}}>|</span> <span style={{ color: "#f05b78"}}>seguir marca</span> </p>

            <h2 className="real-price">R$ { produto.preco } <span className="fake-price">R${produto.preco + 50}</span></h2>
            <h4>10 x R$ { produto.preco / 10 } sem juros</h4>
            
            <div className="buttons">
              <button className="button pink-button">eu quero</button>
              <button className="button white-button">adicionar à sacolinha</button>
              <button className="button white-button">fazer oferta</button>
            </div>
            
            {/*componentizar productDetails = recebe detalhe */}
            <div className="details">
              <div className="size">
                <h4>Tamanho</h4>
                <p>{ detalhe.tamanho ? detalhe.tamanho : `-`}</p>
              </div>
              
              <div className="brand border-gray">
                <h4>Marca</h4>
                <p>{ detalhe.marca ? detalhe.marca : `-`}</p>
              </div>
              <div className="condition border-gray">
                <h4>Condição</h4>
                <p>{ detalhe.condicao ? detalhe.condicao : `-`}</p>
              </div>
              <div className="code border-gray">
                <h4>Codigo</h4>
                <p>{ detalhe.codigo ? detalhe.codigo : `-`}</p>
              </div>
            </div>
          
            <div className="description">
              <h3 className="">descrição</h3>
              <p className="">{detalhe.descricao}</p>
            </div>

            {/* componentizar - sellerInfo = recebe vendedor */}
            <div className="seller-info-container">
              
              <div className="seller-info-1">
                
                <div className="avatar-icon-"></div>
                {console.log(vendedor)}
                
                <div className="name-city">
                  <p>{vendedor.nome}</p>
                  <p>{vendedor.cidade}{ vendedor.estado && `, ${vendedor.estado}` }</p>
                </div>

                <button className="button-seguir" >
                  seguir   
                </button>
              </div>

              <div className="seller-info-2">
                <div  >
                  <h4>avaliação</h4>
                  <p> - </p>
                </div>
                <div>
                  <h4>últimas entregas</h4>
                  <p> - </p>
                </div>
                <div>
                  <h4>tempo médio de envio</h4>
                  <p> {vendedor.tempoMedio} </p>
                </div>
              </div>

              <div className="seller-info-2">
                <div>
                  <h4>à venda</h4>
                  <p> - </p>
                </div>
                <div>
                  <h4>vendidos</h4>
                  <p> {vendedor.qtdItensVendidos} </p>
                </div>
                <div>
                  <h4>no enjoei desde</h4>
                  <p> {this.dataFormat(vendedor.criacao)} </p>
                </div>
              </div>
            </div>

          </div>

          
        </div>

        }
      </>
    );
  }
}

export default ProductUi;