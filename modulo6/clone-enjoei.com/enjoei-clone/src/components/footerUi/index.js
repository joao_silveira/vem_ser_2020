import React from 'react';
import { Layout } from 'antd';

const { Footer } = Layout;

const FooterUi = () => {
  return (
    <>
      <Footer style={{ textAlign: 'center',
        marginTop: '60px', 
        backgroundColor: '#FFF',
        color: '#7d7a77',
        fontSize: '14px', 
        fontWeight: '400', 
        borderTop: '1px solid #f4f2f0' }}>
      enjoei © 2020 - todos os direitos reservados - enjoei.com.br atividades de internet S.A. CNPJ: 16.922.038/0001-51 <br />
      av. isaltino victor de moraes, 437, vila bonfim, embu das artes, sp, 06806-400 - (11) 3197-4883
      </Footer>
    </>
  );
};

export default FooterUi;