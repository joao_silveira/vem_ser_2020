import React, { Component } from 'react';
import { HeaderUi, LoginUi, FooterUi } from '../../components';

class Login extends Component {
  render() {
    return (
      <>
        <HeaderUi isRegisterPage={true} />

        <LoginUi />

        <FooterUi />
      </>
    );
  }
}

export default Login;