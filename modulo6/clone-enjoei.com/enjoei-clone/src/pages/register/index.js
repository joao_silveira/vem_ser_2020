import React, { Component } from 'react';
import { HeaderUi, RegisterUi, FooterUi } from '../../components';


class Register extends Component {
  render() {
    return (
      <>
        <HeaderUi isRegisterPage={true}/>

        <RegisterUi />

        <FooterUi />
      </>
    );
  }
}

export default Register;
