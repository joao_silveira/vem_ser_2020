import React, { Component } from 'react';
import { HeaderUi, ProductUi, FooterUi } from '../../components';

class Product extends Component {
  constructor( props ) {
    super( props );
    
    this.state = {
      idProduto: this.props.location.state.produtoId,
      idVendedor: this.props.location.state.vendedorId
    }
  }

  render() {
    return (
      <>
        <HeaderUi />

        <ProductUi idProduto={ this.state.idProduto } idVendedor={this.state.idVendedor} />

        <FooterUi />
      </>
    );
  }
}

export default Product;