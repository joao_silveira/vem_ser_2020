import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import "antd/dist/antd.css";

import { PrivateRoute } from '../components/privateRoute';
import Home from './home';
import Login from './login';
import Register from './register';
import Product from './product';

export default class App extends Component {  
  render(){
    return ( 
      <Router>
        <Route path="/" exact component={ Home } />
        <Route path="/login" exact component={ Login } />
        <Route path="/register" exact component={ Register } />
        <PrivateRoute path="/produto" exact component ={ Product } />
      </Router>
      )
  };
}
