import React, { Component } from 'react';

import { HeaderUi, CarrouselBannerUi, ContainerProductSquare, FooterUi} from '../../components'

import './index.scss';

export default class Home extends Component {


  render() {
    return (
      <>
        <HeaderUi  />
        <div className="container">
          <CarrouselBannerUi />

          <ContainerProductSquare />
        
          <FooterUi />
        </div>
      </>
    );
  }
}