import Item from 'antd/lib/list/Item';
import Produto from './produto';

export default class ListaProdutos {

	constructor( produtosServidor = [] ){
		this._todos = produtosServidor.map( item => new Produto( item.id, item.idVendedor, item.idCategoria, item.nome, item.preco, item.imagem) );
  
  }
  

  

}