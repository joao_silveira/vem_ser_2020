import axios from "axios";

const url = 'http://localhost:9000/api';

const _get = url => new Promise( ( resolve, reject ) => axios.get( url ).then( response => resolve( response.data ) ) ); 
const _post = ( url, dados ) => new Promise( ( resolve, reject) => axios.post( url, dados ).then( response => resolve( response.data ) ) );

export default class EnjoeiApi {

  async salvaUsuario( nome, email , senha ) {
    const response = await _post( `${ url }/users`, { nome, email , senha } );
    return response[ 0 ];
  }

  async verificaUsuario( email, senha ) {
    const infos = await _get( `${ url }/users?email=${ email }&senha=${ senha }`);
    return infos;
  }

  async buscarVendedores() {
    const response = await _get( `${ url }/vendedor/` );
    return response;
  }

  async buscarVendedor(id) {
    const response = await _get( `${ url }/vendedor/${ id }` );
    return response;
  }

  async buscarProduto( id ) {
    const response = await _get( `${ url }/produtos/${ id }` );
    return response;
  }

  async buscarDetalheDeProduto( id ) {
    const response = await _get( `${ url }/detalhes/?idProduto=${ id }`);
    return response[ 0 ];
  }

  async buscarBanners() {
    return await _get( `${url}/banners` );
  }

  async buscarProdutos() {
    const response = await _get( `${ url }/produtos` );
    return response;
  }

  async buscarProdutosPorCategorias( id ) {
    const response = await _get( `${ url }/produtos?idCategoria=${id}` );
    return response;
  }

  async buscarCategorias() {
    const response = await _get( `${ url }/categorias` );
    return response;
  }

  async buscarDetalhes() {
    const response = await _get( `${ url }/detalhes` )
    return response;
  }

  async registrarComentario( produtoId, comentario , usuario ) {
    const response = await _post( `${ url }/comentarios`, { produtoId, comentario, usuario } );
    return response[ 0 ];
  }

  async buscarComentario( produtoId ) {
    return _get( `${ url }/comentarios?produtoId=${ produtoId }` );
  }

}