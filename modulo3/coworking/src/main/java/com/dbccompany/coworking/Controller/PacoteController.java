package com.dbccompany.coworking.Controller;

import com.dbccompany.coworking.Entity.PacoteEntity;
import com.dbccompany.coworking.Service.PacoteService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/pacote/")
public class PacoteController extends AbstractController<PacoteService, PacoteEntity, Integer> {
}

