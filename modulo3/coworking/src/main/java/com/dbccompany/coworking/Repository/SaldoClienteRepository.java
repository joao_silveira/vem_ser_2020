package com.dbccompany.coworking.Repository;

import com.dbccompany.coworking.Entity.SaldoClienteEntity;
import com.dbccompany.coworking.Entity.SaldoClienteID;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoClienteEntity, Integer> {
    Optional<SaldoClienteEntity> findById(SaldoClienteID newId);
    SaldoClienteEntity findByQuantidade(int qtd);
}
