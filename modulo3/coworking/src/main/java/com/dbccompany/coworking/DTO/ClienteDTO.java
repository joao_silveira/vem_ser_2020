package com.dbccompany.coworking.DTO;

import com.dbccompany.coworking.Entity.*;

import java.util.Date;
import java.util.List;

public class ClienteDTO {
    private Integer id;
    private String nome;
    private String cpf;
    private Date dataNascimento;
    private List<ContatoEntity> contatos;
    private List<SaldoClienteEntity> saldosClientes;
    private List<ClientePacoteEntity> clientesPacotes;
    private List<ContratacaoEntity> contratacao;

    public ClienteDTO() {}

    public ClienteDTO(ClienteEntity cliente) {
        this.id = cliente.getId();
        this.nome = cliente.getNome();
        this.cpf = cliente.getCpf();
        this.dataNascimento = cliente.getDataNascimento();
        this.contatos = cliente.getContatos();
        this.saldosClientes = cliente.getSaldoCliente();
        this.clientesPacotes = cliente.getClientePacote();
        this.contratacao = cliente.getContratacao();
    }

    public ClienteEntity convert() {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setId(this.id);
        cliente.setNome(this.nome);
        cliente.setCpf(this.cpf);
        cliente.setDataNascimento(this.dataNascimento);
        cliente.setContatos(this.contatos);
        cliente.setSaldoCliente(this.saldosClientes);
        cliente.setClientePacote(this.clientesPacotes);
        cliente.setContratacao(this.contratacao);
        return cliente;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<ContatoEntity> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoEntity> contatos) {
        this.contatos = contatos;
    }

    public List<SaldoClienteEntity> getSaldosClientes() {
        return saldosClientes;
    }

    public void setSaldosClientes(List<SaldoClienteEntity> saldosClientes) {
        this.saldosClientes = saldosClientes;
    }

    public List<ClientePacoteEntity> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientePacoteEntity> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<ContratacaoEntity> contratacao) {
        this.contratacao = contratacao;
    }
}
