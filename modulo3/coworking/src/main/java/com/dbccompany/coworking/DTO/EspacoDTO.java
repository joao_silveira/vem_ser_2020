package com.dbccompany.coworking.DTO;

import com.dbccompany.coworking.Entity.ContratacaoEntity;
import com.dbccompany.coworking.Entity.EspacoEntity;
import com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import com.dbccompany.coworking.Entity.SaldoClienteEntity;

import java.util.List;

public class EspacoDTO {

    private Integer id;
    private String nome;
    private int qtdPessoas;
    private String valor;
    private List<SaldoClienteEntity> saldosClientes;
    private List<EspacoPacoteEntity> espacosPacotes;
    private List<ContratacaoEntity> contratacao;

    public EspacoDTO() {}

    public EspacoDTO(EspacoEntity espaco) {
        this.id = espaco.getId();
        this.nome = espaco.getNome();
        this.qtdPessoas = espaco.getQtdPessoas();
        // this.valor = ;
        this.saldosClientes = espaco.getSaldoCliente();
        this.espacosPacotes = espaco.getEspacoPacote();
        this.contratacao = espaco.getContratacao();
    }

    public EspacoEntity convert() {
        EspacoEntity espaco = new EspacoEntity();
        espaco.setId(this.id);
        espaco.setNome(this.nome);
        espaco.setQtdPessoas(this.qtdPessoas);
        // espaco.setValor( tratamento de valor );
        espaco.setSaldoCliente(this.saldosClientes);
        espaco.setEspacoPacote(this.espacosPacotes);
        espaco.setContratacao(this.contratacao);
        return espaco;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(int qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public List<SaldoClienteEntity> getSaldosClientes() {
        return saldosClientes;
    }

    public void setSaldosClientes(List<SaldoClienteEntity> saldosClientes) {
        this.saldosClientes = saldosClientes;
    }

    public List<EspacoPacoteEntity> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacoPacoteEntity> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<ContratacaoEntity> contratacao) {
        this.contratacao = contratacao;
    }
}
