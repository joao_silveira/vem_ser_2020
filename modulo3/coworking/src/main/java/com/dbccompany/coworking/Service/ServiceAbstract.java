package com.dbccompany.coworking.Service;

import com.dbccompany.coworking.CoworkingApplication;
import com.dbccompany.coworking.Entity.EntityAbstract;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public abstract class ServiceAbstract<
        R extends CrudRepository<E, T>,
        E extends EntityAbstract, T> {

    Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @Autowired
    R repository;

    @Transactional( rollbackFor = Exception.class)
    public E salvar( E entidade ) {
        try {
            return repository.save(entidade);
        } catch (Exception err) {
            logger.error("Erro ao salvar:" + err.getMessage());
            throw new RuntimeException();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public E editar( E entidade, T id ) {
        try {
            entidade.setId(id);
            return repository.save(entidade);
        } catch (Exception err) {
            logger.error("Erro ao editar:" + err.getMessage());
            throw new RuntimeException();
        }
    }

    public List<E> todos() {
        try {
            Iterable<E> all = repository.findAll();

            if(all == null) {
                logger.warn("Nao existe cadastro");
            }
            return (List<E>) all;
        } catch (Exception err) {
            throw new RuntimeException();
        }
    }

    public E porId( T id) {
        try {
            E temp = repository.findById(id).get();
            if(temp == null){
                logger.warn("Nao existe cadastro com o ID fornecido");
            }
            return temp;
        } catch (Exception err) {
            throw new RuntimeException();
        }
    }

}
