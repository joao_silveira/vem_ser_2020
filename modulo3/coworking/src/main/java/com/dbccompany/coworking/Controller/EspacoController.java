package com.dbccompany.coworking.Controller;

import com.dbccompany.coworking.Entity.EspacoEntity;
import com.dbccompany.coworking.Service.EspacoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/espaco/")
public class EspacoController extends AbstractController<EspacoService, EspacoEntity, Integer> {
}

