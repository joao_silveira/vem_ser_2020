package com.dbccompany.coworking.DTO;

import com.dbccompany.coworking.Entity.*;

import java.util.Date;
import java.util.List;

public class SaldoClienteDTO {
    private SaldoClienteID id;
    private TipoContratacaoEnum tipoContratacao;
    private int quantidade;
    private Date vencimento;
    private ClienteEntity cliente;
    private EspacoEntity espaco;
    private List<AcessoEntity> acessos;

    public SaldoClienteDTO() {}

    public SaldoClienteDTO(SaldoClienteEntity saldoCliente) {
        this.id = saldoCliente.getId();
        this.tipoContratacao = saldoCliente.getTipoContratacao();
        this.quantidade = saldoCliente.getQuantidade();
        this.vencimento = saldoCliente.getDataVencimento();
        this.cliente = saldoCliente.getCliente();
        this.espaco = saldoCliente.getEspaco();
        this.acessos = saldoCliente.getAcesso();
    }

    public SaldoClienteEntity convert() {
        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId(this.id);
        saldoCliente.setTipoContratacao(this.tipoContratacao);
        saldoCliente.setQuantidade(this.quantidade);
        saldoCliente.setDataVencimento(this.vencimento);
        saldoCliente.setCliente(this.cliente);
        saldoCliente.setEspaco(this.espaco);
        saldoCliente.setAcesso(this.acessos);
        return saldoCliente;
    }

    public SaldoClienteID getId() {
        return id;
    }

    public void setId(SaldoClienteID id) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public List<AcessoEntity> getAcessos() {
        return acessos;
    }

    public void setAcessos(List<AcessoEntity> acessos) {
        this.acessos = acessos;
    }
}
