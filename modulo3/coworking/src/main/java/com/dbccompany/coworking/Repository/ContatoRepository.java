package com.dbccompany.coworking.Repository;

import com.dbccompany.coworking.Entity.ContatoEntity;
import com.dbccompany.coworking.Entity.TipoContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContatoRepository extends CrudRepository<ContatoEntity, Integer> {
    ContatoEntity findByValor( String valor );
    List<ContatoEntity> findAllByTipo(TipoContatoEntity tipoCt);
}
