package com.dbccompany.coworking.Repository;

import com.dbccompany.coworking.Entity.PagamentoEntity;
import com.dbccompany.coworking.Entity.TipoPagamentoEnum;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<PagamentoEntity, Integer> {
    List<PagamentoEntity> findAllByTipoPagamento(TipoPagamentoEnum tipo );
}
