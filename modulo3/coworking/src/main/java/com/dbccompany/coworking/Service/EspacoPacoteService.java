package com.dbccompany.coworking.Service;

import com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import com.dbccompany.coworking.Repository.EspacoPacoteRepository;
import org.springframework.stereotype.Service;

@Service
public class EspacoPacoteService extends ServiceAbstract<EspacoPacoteRepository, EspacoPacoteEntity, Integer>{
}
