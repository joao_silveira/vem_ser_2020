package com.dbccompany.coworking.Repository;

import com.dbccompany.coworking.Entity.ClienteEntity;
import com.dbccompany.coworking.Entity.ClientePacoteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends CrudRepository<ClienteEntity, Integer> {
    ClienteEntity findByCpf( String cpf );
}