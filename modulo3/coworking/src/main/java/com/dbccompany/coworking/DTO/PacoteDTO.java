package com.dbccompany.coworking.DTO;

import com.dbccompany.coworking.Entity.ClientePacoteEntity;
import com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import com.dbccompany.coworking.Entity.PacoteEntity;

import java.util.List;

public class PacoteDTO {
    private Integer id;
    private List<EspacoPacoteEntity> espacosPacotes;
    private String valor;
    private List<ClientePacoteEntity> clientesPacotes;

    public PacoteDTO() {}

    public PacoteDTO(PacoteEntity pacote) {
        this.id = pacote.getId();
        this.espacosPacotes = pacote.getEspacoPacote();
        // this.valor tratamento de valores
        this.clientesPacotes = pacote.getClientePacote();
    }

    public PacoteEntity convert() {
        PacoteEntity pacote = new PacoteEntity();
        pacote.setId(this.id);
        pacote.setEspacoPacote(this.espacosPacotes);
        //pacote.setValor( tratamento de valor );
        pacote.setClientePacote(this.clientesPacotes);
        return pacote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<EspacoPacoteEntity> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacoPacoteEntity> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public List<ClientePacoteEntity> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientePacoteEntity> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }
}
