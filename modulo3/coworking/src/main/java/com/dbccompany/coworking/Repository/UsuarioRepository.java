package com.dbccompany.coworking.Repository;

import com.dbccompany.coworking.Entity.UsuarioEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsuarioRepository extends CrudRepository<UsuarioEntity, Integer> {
    UsuarioEntity findByLoginAndSenha( String login, String senha);

    Optional<UsuarioEntity> findByLogin(String login);

    //UsuarioEntity findByLogin(String login);
}