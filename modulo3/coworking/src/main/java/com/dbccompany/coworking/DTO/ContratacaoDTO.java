package com.dbccompany.coworking.DTO;

import com.dbccompany.coworking.Entity.*;

import java.util.List;

public class ContratacaoDTO {
    private Integer id;
    private ClienteEntity cliente;
    private EspacoEntity espaco;
    private TipoContratacaoEnum tipoContratacao;
    private int quantidade;
    private double desconto = 0.0;
    private int prazo;
    private List<PagamentoEntity> pagamentos;
    private double valorContratado;



    public ContratacaoDTO() {}

    public ContratacaoDTO(ContratacaoEntity contratacao) {
        this.id = contratacao.getId();
        this.cliente = contratacao.getCliente();
        this.espaco = contratacao.getEspaco();
        this.tipoContratacao = contratacao.getTipoContratacao();
        this.quantidade = contratacao.getQuantidade();
        this.desconto = contratacao.getDesconto();
        this.prazo = contratacao.getPrazo();
        this.pagamentos = contratacao.getPagamento();
        this.valorContratado = contratacao.getEspaco().getValor() - this.desconto;
    }

    public ContratacaoEntity convert() {
        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setId(this.id);
        contratacao.setCliente(this.cliente);
        contratacao.setEspaco(this.espaco);
        contratacao.setTipoContratacao(this.tipoContratacao);
        contratacao.setQuantidade(this.quantidade);
        contratacao.setDesconto(this.desconto);
        contratacao.setPrazo(this.prazo);
        contratacao.setPagamento(this.pagamentos);
        return contratacao;
    }
}
