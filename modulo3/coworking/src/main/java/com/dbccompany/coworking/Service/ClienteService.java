package com.dbccompany.coworking.Service;

import com.dbccompany.coworking.DTO.ClienteDTO;
import com.dbccompany.coworking.Entity.ClienteEntity;
import com.dbccompany.coworking.Entity.ContatoEntity;
import com.dbccompany.coworking.Repository.ClienteRepository;
import com.dbccompany.coworking.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClienteService extends ServiceAbstract  <ClienteRepository, ClienteEntity, Integer> {
    @Autowired
    ContatoRepository contatoRepository;

    public ClienteDTO salvarComContatos(ClienteDTO cliente ) {
        List<ContatoEntity> contato = new ArrayList<>();
        contato.add(validacaoContato("email"));
        contato.add(validacaoContato("telefone"));

        cliente.setContatos(contato);
        ClienteEntity clienteFinal = cliente.convert();
        ClienteDTO newDTO = new ClienteDTO(repository.save(clienteFinal));
        return newDTO;
    }

    private ContatoEntity validacaoContato( String nome ) {

        ContatoEntity contato = contatoRepository.findByValor(nome);
        contato = (contato == null) ? contatoRepository.save(new ContatoEntity()) : contato ;
        return contato;
    }

}