package com.dbccompany.coworking.Service;

import com.dbccompany.coworking.Entity.ContratacaoEntity;
import com.dbccompany.coworking.Repository.ContratacaoRepository;
import org.springframework.stereotype.Service;

@Service
public class ContratacaoService extends ServiceAbstract<ContratacaoRepository, ContratacaoEntity, Integer> {
}
