package com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

import static com.dbccompany.coworking.Util.ValorConversor.converteDoubleToString;
import static com.dbccompany.coworking.Util.ValorConversor.converteStringToDouble;

@Entity
public class EspacoEntity extends EntityAbstract<Integer>{

    @Id
    @SequenceGenerator(name = "ESPACO_SEQ", sequenceName = "ESPACO_SEQ")
    @GeneratedValue( generator = "ESPACO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column( unique = true, nullable = false )
    private String nome;

    @Column( nullable = false )
    private int qtdPessoas;

    @Column( nullable = false )
    private double valor;

    @OneToMany(mappedBy = "espaco")
    private List<EspacoPacoteEntity> espacoPacote;

    @OneToMany(mappedBy = "espaco")
    private List<SaldoClienteEntity> saldoCliente;

    @OneToMany(mappedBy = "espaco")
    private List<ContratacaoEntity> contratacao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(int qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public double getValor() {
        return this.valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public List<EspacoPacoteEntity> getEspacoPacote() {
        return espacoPacote;
    }

    public void setEspacoPacote(List<EspacoPacoteEntity> espacoPacote) {
        this.espacoPacote = espacoPacote;
    }

    public List<SaldoClienteEntity> getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(List<SaldoClienteEntity> saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<ContratacaoEntity> contratacao) {
        this.contratacao = contratacao;
    }
}
