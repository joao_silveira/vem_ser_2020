package com.dbccompany.coworking.Repository;

import com.dbccompany.coworking.Entity.ClientePacoteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientePacoteRepository extends CrudRepository<ClientePacoteEntity, Integer> {
}