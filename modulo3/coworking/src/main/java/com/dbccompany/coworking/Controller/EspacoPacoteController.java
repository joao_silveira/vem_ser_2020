package com.dbccompany.coworking.Controller;

import com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import com.dbccompany.coworking.Service.EspacoPacoteService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/espacopacote/")
public class EspacoPacoteController extends AbstractController<EspacoPacoteService, EspacoPacoteEntity, Integer> {
}

