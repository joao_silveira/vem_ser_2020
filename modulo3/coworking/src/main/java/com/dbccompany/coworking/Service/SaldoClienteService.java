package com.dbccompany.coworking.Service;

import com.dbccompany.coworking.Entity.SaldoClienteEntity;
import com.dbccompany.coworking.Entity.SaldoClienteID;
import com.dbccompany.coworking.Repository.SaldoClienteRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class SaldoClienteService extends ServiceAbstract<SaldoClienteRepository, SaldoClienteEntity, Integer>{
    @Transactional(rollbackFor = Exception.class)
    public SaldoClienteEntity editar(SaldoClienteEntity entidade, Integer idCliente, Integer idEspaco) {
        SaldoClienteID newId = new SaldoClienteID(idCliente, idEspaco);
        entidade.setId(newId);

        return repository.save(entidade);
    }

    @Transactional(rollbackFor = Exception.class)
    public Optional<SaldoClienteEntity> porId(Integer idCliente, Integer idEspaco) {
        SaldoClienteID newId = new SaldoClienteID(idCliente, idEspaco);
        Optional<SaldoClienteEntity> optionalEntity = repository.findById(newId);
        return optionalEntity;
    }

}