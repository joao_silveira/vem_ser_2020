package com.dbccompany.coworking.Controller;

import com.dbccompany.coworking.Entity.SaldoClienteEntity;
import com.dbccompany.coworking.Service.SaldoClienteService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/saldosliente/")
public class SaldoClienteController extends AbstractController<SaldoClienteService, SaldoClienteEntity, Integer> {
}

