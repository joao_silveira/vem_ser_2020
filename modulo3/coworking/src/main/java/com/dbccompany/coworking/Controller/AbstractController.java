package com.dbccompany.coworking.Controller;

import com.dbccompany.coworking.CoworkingApplication;
import com.dbccompany.coworking.Entity.EntityAbstract;
import com.dbccompany.coworking.Service.ServiceAbstract;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public abstract class AbstractController<S extends ServiceAbstract, E extends EntityAbstract, T>{

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @Autowired
    protected S service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<E> todos() {
        return service.todos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public E salvar(@RequestBody E entity){
        return (E) service.salvar(entity);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public E espeficifico(@PathVariable T id) {
        return (E) service.porId(id);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public E editar(@PathVariable T id, @RequestBody E entity) {
        return (E) service.editar(entity, id);
    }
}


