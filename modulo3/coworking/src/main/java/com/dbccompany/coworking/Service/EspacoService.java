package com.dbccompany.coworking.Service;

import com.dbccompany.coworking.Entity.EspacoEntity;
import com.dbccompany.coworking.Repository.EspacoRepository;
import org.springframework.stereotype.Service;

@Service
public class EspacoService extends ServiceAbstract<EspacoRepository, EspacoEntity, Integer> {
}
