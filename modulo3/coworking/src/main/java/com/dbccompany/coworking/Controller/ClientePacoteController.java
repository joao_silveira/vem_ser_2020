package com.dbccompany.coworking.Controller;

import com.dbccompany.coworking.Entity.ClientePacoteEntity;
import com.dbccompany.coworking.Service.ClientePacoteService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/clientepacote/")
public class ClientePacoteController extends AbstractController<ClientePacoteService, ClientePacoteEntity, Integer> {
}
