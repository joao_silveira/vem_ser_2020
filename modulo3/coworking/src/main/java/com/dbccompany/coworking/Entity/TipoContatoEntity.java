package com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class TipoContatoEntity extends EntityAbstract<Integer>{

    @Id
    @SequenceGenerator(name = "TIPOCONTATO_SEQ", sequenceName = "TIPOCONTATO_SEQ")
    @GeneratedValue( generator = "TIPOCONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column( unique = true, nullable = false )
    private String nome;

    @OneToMany( mappedBy = "tipo")
    private List<ContatoEntity> contato;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<ContatoEntity> getContatos() {
        return contato;
    }

    public void setContatos(List<ContatoEntity> contatos) {
        this.contato = contatos;
    }

}

