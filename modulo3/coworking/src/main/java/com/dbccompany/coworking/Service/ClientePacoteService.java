package com.dbccompany.coworking.Service;

import com.dbccompany.coworking.Entity.ClientePacoteEntity;
import com.dbccompany.coworking.Repository.ClientePacoteRepository;
import org.springframework.stereotype.Service;


@Service
public class ClientePacoteService extends ServiceAbstract  <ClientePacoteRepository, ClientePacoteEntity, Integer> {

}
