package com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "SALDO_CLIENTE")
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = SaldoClienteEntity.class)
public class SaldoClienteEntity extends EntityAbstract<SaldoClienteID>{

    @EmbeddedId
    private SaldoClienteID id;

    @Enumerated( EnumType.STRING)
    private TipoContratacaoEnum tipoContratacao;

    @Column(nullable = false)
    private int quantidade;

    @Column(nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yy")
    private Date dataVencimento;

    @ManyToOne // insertable e updatable adicionados por correcao da IDE
    @JoinColumn(name = "ID_CLIENTE", nullable = false, insertable = false, updatable = false)
    private ClienteEntity cliente;

    @ManyToOne // insertable e updatable adicionados por correcao da IDE
    @JoinColumn(name = "ID_ESPACO", nullable = false, insertable = false, updatable = false)
    private EspacoEntity espaco;

    @OneToMany(mappedBy = "saldoCliente")
    private List<AcessoEntity> acesso;

    public SaldoClienteID getId() {
        return id;
    }

    public void setId(SaldoClienteID id) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Date getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(Date dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public List<AcessoEntity> getAcesso() {
        return acesso;
    }

    public void setAcesso(List<AcessoEntity> acesso) {
        this.acesso = acesso;
    }
}
