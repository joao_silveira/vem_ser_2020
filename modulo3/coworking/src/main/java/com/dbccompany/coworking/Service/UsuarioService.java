package com.dbccompany.coworking.Service;

import com.dbccompany.coworking.DTO.UsuarioDTO;
import com.dbccompany.coworking.Entity.UsuarioEntity;
import com.dbccompany.coworking.Repository.UsuarioRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class UsuarioService extends ServiceAbstract<UsuarioRepository, UsuarioEntity, Integer> {

    @Override
    @Transactional(rollbackFor = Exception.class)
    public UsuarioEntity salvar(UsuarioEntity usuario) {
        String senha = usuario.getSenha();
        if(senha.length() >= 6){
            usuario.setSenha(new BCryptPasswordEncoder().encode(usuario.getSenha()));
            return repository.save(usuario);
        }
        return null;
    }

    public List<UsuarioDTO> retornarListaUsuarios() {
        List<UsuarioDTO> listaDTO = new ArrayList<>();
        for (UsuarioEntity usuario : this.todos()) {
            listaDTO.add(new UsuarioDTO(usuario));
        }

        return listaDTO;
    }

    public boolean login( String login, String senha ) {
        UsuarioEntity usuario = repository.findByLoginAndSenha(login, senha);
        return usuario == null ? false : true;
    }

}
