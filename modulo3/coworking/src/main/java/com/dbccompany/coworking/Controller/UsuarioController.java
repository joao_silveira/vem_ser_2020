package com.dbccompany.coworking.Controller;

import com.dbccompany.coworking.CoworkingApplication;
import com.dbccompany.coworking.DTO.UsuarioDTO;
import com.dbccompany.coworking.Entity.UsuarioEntity;
import com.dbccompany.coworking.Service.UsuarioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/usuario/")
public class UsuarioController extends AbstractController<UsuarioService, UsuarioEntity, Integer> {

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @PostMapping( value = "/login")
    @ResponseBody
    public boolean logar(@RequestBody UsuarioDTO login){
        logger.info("Tentando logar usuario...");
        return service.login(login.getLogin(), login.getSenha());
    }

    /*
    @GetMapping(value = "/todos")
    @ResponseBody
    public List<UsuarioDTO> todosUsuarios() {
        return service.retornarListaUsuarios();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public UsuarioDTO salvar(@RequestBody UsuarioDTO user) {
        logger.info("Salvando usuario...");
        UsuarioEntity userE = user.convert();
        service.salvar(userE);
        return user;
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public UsuarioEntity espeficifico(@PathVariable Integer id) {
        logger.info("Buscando usuario por ID");

        return service.porId(id);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public UsuarioDTO editar(@PathVariable Integer id, @RequestBody UsuarioDTO user) {
        logger.info("Editando usuario...");

        UsuarioEntity userE = user.convert();

        service.editar(userE, id);

        return user;
    }


     */

}
