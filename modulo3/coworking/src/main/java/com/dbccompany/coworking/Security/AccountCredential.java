package com.dbccompany.coworking.Security;

import com.dbccompany.coworking.Entity.UsuarioEntity;

public class AccountCredential {

    private UsuarioEntity usuarioEntity;

    private String username;
    private String password;

    public AccountCredential() {}

    public AccountCredential(UsuarioEntity user) {
        this.usuarioEntity = user;
        this.username = user.getLogin();
        this.password = user.getPassword();
    }

    public UsuarioEntity getUsuarioEntity() {
        return usuarioEntity;
    }

    public void setUsuarioEntity(UsuarioEntity usuarioEntity) {
        this.usuarioEntity = usuarioEntity;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}