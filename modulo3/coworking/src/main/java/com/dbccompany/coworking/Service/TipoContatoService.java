package com.dbccompany.coworking.Service;

import com.dbccompany.coworking.Entity.TipoContatoEntity;
import com.dbccompany.coworking.Repository.TipoContatoRepository;
import org.springframework.stereotype.Service;

@Service
public class TipoContatoService extends ServiceAbstract<TipoContatoRepository, TipoContatoEntity, Integer>{
}