package com.dbccompany.coworking.Controller;

import com.dbccompany.coworking.Entity.TipoContatoEntity;
import com.dbccompany.coworking.Service.TipoContatoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/contatotipo/")
public class TipoContatoController extends AbstractController<TipoContatoService, TipoContatoEntity, Integer> {


}