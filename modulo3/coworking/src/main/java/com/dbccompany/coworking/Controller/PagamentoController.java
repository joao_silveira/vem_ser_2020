package com.dbccompany.coworking.Controller;

import com.dbccompany.coworking.Entity.PagamentoEntity;
import com.dbccompany.coworking.Service.PagamentoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/pagamento/")
public class PagamentoController extends AbstractController<PagamentoService, PagamentoEntity, Integer> {
}
