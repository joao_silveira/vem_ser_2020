package com.dbccompany.coworking.Repository;

import com.dbccompany.coworking.Entity.AcessoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AcessoRepository extends CrudRepository<AcessoEntity, Integer> {

}
