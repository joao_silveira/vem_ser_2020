package com.dbccompany.coworking.Repository;


import com.dbccompany.coworking.Entity.ContratacaoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContratacaoRepository extends CrudRepository<ContratacaoEntity, Integer> {
}
