package com.dbccompany.coworking.Controller;

import com.dbccompany.coworking.Entity.ContatoEntity;
import com.dbccompany.coworking.Service.ContatoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/contato/")
public class ContatoController extends AbstractController<ContatoService, ContatoEntity, Integer> {
}