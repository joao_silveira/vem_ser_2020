package com.dbccompany.coworking.Repository;

import com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EspacoPacoteRepository extends CrudRepository<EspacoPacoteEntity, Integer> {
}
