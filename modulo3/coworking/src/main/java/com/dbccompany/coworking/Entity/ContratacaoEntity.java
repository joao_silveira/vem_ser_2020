package com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.List;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = ContatoEntity.class)
public class ContratacaoEntity extends EntityAbstract<Integer>{

    @Id
    @SequenceGenerator(name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
    @GeneratedValue( generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne //( cascade = CascadeType.ALL )
    @JoinColumn( name = "ID_ESPACO")
    private EspacoEntity espaco;

    @ManyToOne //( cascade = CascadeType.ALL )
    @JoinColumn( name = "ID_CLIENTE")
    private ClienteEntity cliente;

    @OneToMany(mappedBy = "contratacao")
    private List<PagamentoEntity> pagamento;

    @Enumerated( EnumType.STRING)
    private TipoContratacaoEnum tipoContratacao;

    @Column( nullable = false)
    private int quantidade;

    @Column( nullable = true)
    private double desconto;

    @Column( nullable = false)
    private int prazo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getDesconto() {
        return desconto;
    }

    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }

    public List<PagamentoEntity> getPagamento() {
        return pagamento;
    }

    public void setPagamento(List<PagamentoEntity> pagamento) {
        this.pagamento = pagamento;
    }
}
