package com.dbccompany.coworking.Service;

import com.dbccompany.coworking.DTO.AcessoDTO;
import com.dbccompany.coworking.Entity.AcessoEntity;
import com.dbccompany.coworking.Entity.SaldoClienteEntity;
import com.dbccompany.coworking.Entity.SaldoClienteID;
import com.dbccompany.coworking.Repository.AcessoRepository;
import com.dbccompany.coworking.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AcessoService extends ServiceAbstract  <AcessoRepository, AcessoEntity, Integer> {

    @Autowired
    SaldoClienteRepository saldoRepository;

    public AcessoDTO salvarEntrada( AcessoDTO acesso ){
        if( acesso.isEntrada() ) {
            SaldoClienteID id = acesso.getSaldoCliente().getId();
            SaldoClienteEntity saldo = saldoRepository.findById(id).get();
            if(saldo.getQuantidade() <= 0) {
                return null;
            }
        }
        AcessoEntity acessoEntity = acesso.convert();
        AcessoDTO newDto = new AcessoDTO(super.salvar(acessoEntity));
        return newDto;
    }

}
