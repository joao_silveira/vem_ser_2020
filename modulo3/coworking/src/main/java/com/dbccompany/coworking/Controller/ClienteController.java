package com.dbccompany.coworking.Controller;

import com.dbccompany.coworking.Entity.ClienteEntity;
import com.dbccompany.coworking.Service.ClienteService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/cliente/")
public class ClienteController extends AbstractController<ClienteService, ClienteEntity, Integer> {
}
