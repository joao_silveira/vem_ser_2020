package com.dbccompany.coworking.Repository;

import com.dbccompany.coworking.Entity.EspacoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EspacoRepository extends CrudRepository<EspacoEntity, Integer> {
    EspacoEntity findByQtdPessoas(int qtd);
}