package com.dbccompany.coworking.Repository;

import com.dbccompany.coworking.Entity.PacoteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PacoteRepository extends CrudRepository<PacoteEntity, Integer> {
}
