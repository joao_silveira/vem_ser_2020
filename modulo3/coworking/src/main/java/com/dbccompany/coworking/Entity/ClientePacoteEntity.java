package com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class ClientePacoteEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "CLIENTE_PACOTE_SEQ", sequenceName = "CLIENTE_PACOTE_SEQ")
    @GeneratedValue( generator = "CLIENTE_PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @OneToMany(mappedBy = "clientePacote")
    private List<PagamentoEntity> pagamento;

    @ManyToOne
    @JoinColumn( name = "ID_CLIENTE", nullable = false)
    private ClienteEntity cliente;

    @ManyToOne
    @JoinColumn( name = "ID_PACOTE", nullable = false)
    private PacoteEntity pacote;

    private int quantidade;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public PacoteEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacoteEntity pacote) {
        this.pacote = pacote;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public List<PagamentoEntity> getPagamento() {
        return pagamento;
    }

    public void setPagamento(List<PagamentoEntity> pagamento) {
        this.pagamento = pagamento;
    }

}
