package com.dbccompany.coworking.Util;

public class ValorConversor {

    public static double converteStringToDouble(String valor){
        valor = valor.substring(2);
        return Double.parseDouble(valor);
    }

    public static String converteDoubleToString(double valor){
        return "R$"+valor;
    }

}
