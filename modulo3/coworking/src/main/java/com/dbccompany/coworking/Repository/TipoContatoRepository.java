package com.dbccompany.coworking.Repository;


import com.dbccompany.coworking.Entity.ContatoEntity;
import com.dbccompany.coworking.Entity.TipoContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface TipoContatoRepository extends CrudRepository<TipoContatoEntity, Integer> {
    List<ContatoEntity> findAllByNome(ContatoEntity ct);

}
