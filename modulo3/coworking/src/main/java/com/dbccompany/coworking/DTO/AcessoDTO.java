package com.dbccompany.coworking.DTO;

import com.dbccompany.coworking.Entity.AcessoEntity;
import com.dbccompany.coworking.Entity.SaldoClienteEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;

public class AcessoDTO {
    private Integer id;
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private SaldoClienteEntity saldoCliente;
    private boolean isEntrada;
    private Date data;
    private boolean isExcecao;
    private long timeEntrada;
    private long timeSaida;

    {
        this.isEntrada = false;
        this.isExcecao = false;
    }

    public AcessoDTO() {}

    public AcessoDTO(AcessoEntity acesso) {
        this.id = acesso.getId();
        this.saldoCliente = acesso.getSaldoCliente();
        this.isEntrada = acesso.isEntrada();
        this.data = acesso.getData();
        this.isExcecao = acesso.isExcecao();
    }

    public AcessoEntity convert() {
        AcessoEntity acesso = new AcessoEntity();
        acesso.setId(this.id);
        acesso.setSaldoCliente(this.saldoCliente);
        acesso.setEntrada(this.isEntrada);
        acesso.setData(this.data);
        acesso.setExcecao(this.isExcecao);
        return acesso;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SaldoClienteEntity getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoClienteEntity saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public boolean isEntrada() {
        return isEntrada;
    }

    public void setEntrada(boolean entrada) {
        isEntrada = entrada;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public boolean isExcecao() {
        return isExcecao;
    }

    public void setExcecao(boolean excecao) {
        isExcecao = excecao;
    }

    public long getTimeEntrada() {
        return timeEntrada;
    }

    public void setTimeEntrada(long timeEntrada) {
        this.timeEntrada = timeEntrada;
    }

    public long getTimeSaida() {
        return timeSaida;
    }

    public void setTimeSaida(long timeSaida) {
        this.timeSaida = timeSaida;
    }
}
