package com.dbccompany.coworking.Controller;

import com.dbccompany.coworking.Entity.ContratacaoEntity;
import com.dbccompany.coworking.Service.ContratacaoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/contratacao/")
public class ContratacaoController extends AbstractController<ContratacaoService, ContratacaoEntity, Integer> {
}
