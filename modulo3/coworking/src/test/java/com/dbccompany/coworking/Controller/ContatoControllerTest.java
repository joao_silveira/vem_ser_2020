package com.dbccompany.coworking.Controller;

import com.dbccompany.coworking.Entity.ContatoEntity;
import com.dbccompany.coworking.Entity.TipoContatoEntity;
import com.dbccompany.coworking.Entity.UsuarioEntity;
import com.dbccompany.coworking.Repository.ContatoRepository;
import com.dbccompany.coworking.Repository.TipoContatoRepository;
import com.dbccompany.coworking.Repository.UsuarioRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class ContatoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ContatoRepository contatoRepository;

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Test
    public void deveRetornar200ConsultandoTipoContatoEntity () throws  Exception {
        URI uri = new URI("/api/contatotipo/todos");

        mockMvc
                .perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status()
                        .is(200)
                );
    }

    @Test
    public void deveRetornarUmTipoContatoEntity() throws Exception {
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("fumaca");

        TipoContatoEntity newTipocontato = tipoContatoRepository.save(tipoContato);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/contatotipo/ver/{id}", newTipocontato.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                )
        .andExpect(MockMvcResultMatchers
                .jsonPath("$.nome")
                .value("fumaca")
        );
    }

    @Test
    public void deveRetornarUmContatoEntity() throws Exception {
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("fumaca");

        TipoContatoEntity newTipocontato = tipoContatoRepository.save(tipoContato);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/contatotipo/ver/{id}", newTipocontato.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.nome")
                        .value("fumaca")
                );
    }


}