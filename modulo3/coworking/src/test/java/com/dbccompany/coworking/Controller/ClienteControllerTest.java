package com.dbccompany.coworking.Controller;

import com.dbccompany.coworking.Entity.TipoContatoEntity;
import com.dbccompany.coworking.Repository.ClienteRepository;
import com.dbccompany.coworking.Repository.ContatoRepository;
import com.dbccompany.coworking.Repository.TipoContatoRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ClienteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ClienteRepository cliRepo;

    @Autowired
    private TipoContatoRepository tipoContatoRepo;

    @Autowired
    private ContatoRepository contatoRepo;


    @Test
    public void deveRetornar200TodosClienteEntity () throws  Exception {
        URI uri = new URI("/api/cliente/todos");

        mockMvc
                .perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status()
                        .is(200)
                );
    }

    //erro
  @Test
  public void salvarRetornarUmClienteEntity() throws Exception {

      URI uri = new URI("/api/cliente/novo");

      String json = "{\"nome\": \"jv\"," +
              "\"cpf\": \"12345678901\"," +
              "\"dataNascimento\": \"29/06/2000\"";

      mockMvc
              .perform(MockMvcRequestBuilders
                      .post(uri)
                      .content(json)
                      .contentType(MediaType.APPLICATION_JSON)
              ).andExpect(MockMvcResultMatchers
              .jsonPath("$.nome").value("fulano")
      );
  }



}
