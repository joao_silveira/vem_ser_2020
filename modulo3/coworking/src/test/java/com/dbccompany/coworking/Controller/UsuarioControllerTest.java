package com.dbccompany.coworking.Controller;

import com.dbccompany.coworking.Entity.UsuarioEntity;
import com.dbccompany.coworking.Repository.UsuarioRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class UsuarioControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UsuarioRepository userRepo;

    @Test
    public void deveRetornar200ConsultandoUsuario() throws Exception {
        URI uri = new URI("/api/usuario/todos");

        mockMvc
                .perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status()
                        .is(200)
                );
    }

    @Test
    public void salvarERetornarUmUsuario() throws Exception {
        URI uri = new URI("/api/usuario/novo");
        String json = "{ \"nome\": \"Joao\", " +
                "\"email\": \"pate@maionese.com\"," +
                "\"login\": \"elfo02\"," +
                "\"senha\": \"123123\"  }";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                        .jsonPath("$.id").exists()
                )
        .andExpect(MockMvcResultMatchers
                        .jsonPath("$.nome").value("Joao")
        );
    }

    @Test
    public void deveRetornarUmUsuario() throws Exception {
        UsuarioEntity user = new UsuarioEntity();
        user.setEmail("jv@jv");
        user.setNome("jv");
        user.setLogin("jv");
        user.setSenha("123");
        UsuarioEntity newUsuario = userRepo.save(user);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/usuario/ver/{id}", newUsuario.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome")
                .value("jv")
        );
    }

    //erro
    @Test
    public void deveEditarUmUsuario() throws Exception {
        UsuarioEntity usuario = new UsuarioEntity();
        usuario.setEmail("jv@jv");
        usuario.setNome("jv");
        usuario.setLogin("jv");
        usuario.setSenha("123");
        UsuarioEntity newUser = userRepo.save(usuario);

        String json = "{\"nome\": \"maria\"," +
                "\"email\": \"maria@gmail.com\"," +
                "\"login\": \"maria\"," +
                "\"senha\": \"123\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put("/api/usuario/editar/{id}", newUser.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE).content(json)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome")
                .value("maria")
        );
    }
}