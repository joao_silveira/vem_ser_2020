package com.dbccompany.coworking.Repository;

import com.dbccompany.coworking.Entity.UsuarioEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class UsuarioRepositoryTest {

    @Autowired
    private UsuarioRepository repo;

    @Test
    public void salvaUsuarioEVerificaDadosPorLoginESenha() {
        UsuarioEntity user = new UsuarioEntity();

        user.setNome("Dwarf");
        user.setLogin("Elfo");
        user.setEmail("ElfoNegro@dwarf.com");
        user.setSenha("Mago123");

        repo.save(user);

        assertEquals(user.getNome(), repo.findByLoginAndSenha("Elfo", "Mago123").getNome());
        assertEquals(user.getEmail(), repo.findByLoginAndSenha("Elfo", "Mago123").getEmail());
        assertEquals(user.getLogin(), repo.findByLoginAndSenha("Elfo", "Mago123").getLogin());
        assertEquals(user.getSenha(), repo.findByLoginAndSenha("Elfo", "Mago123").getSenha());
    }

    @Test
    public void salvaUsuarioEExcluiPorLoginESenha() {
        UsuarioEntity user = new UsuarioEntity();

        user.setNome("Dwarf");
        user.setLogin("Elfo");
        user.setEmail("ElfoNegro@dwarf.com");
        user.setSenha("Mago123");

        repo.save(user);

        assertNotNull(repo.findByLoginAndSenha("Elfo", "Mago123"));

        repo.delete(repo.findByLoginAndSenha("Elfo", "Mago123"));

        assertNull(repo.findByLoginAndSenha("Elfo", "Mago123"));

    }

    @Test
    public void BuscarUsuarioInexistentePorLoginAndSenha() {
        assertNull(repo.findByLoginAndSenha("Elfo", "Mago123"));
    }



}
