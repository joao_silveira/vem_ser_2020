package com.dbccompany.coworking.Controller;

import com.dbccompany.coworking.Entity.*;
import com.dbccompany.coworking.Repository.AcessoRepository;
import com.dbccompany.coworking.Repository.ClienteRepository;
import com.dbccompany.coworking.Repository.EspacoRepository;
import com.dbccompany.coworking.Repository.SaldoClienteRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class AcessoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AcessoRepository acessoRepo;

    @Autowired
    private ClienteRepository clienteRepo;

    @Autowired
    private EspacoRepository espacoRepo;

    @Autowired
    private SaldoClienteRepository saldoClienteRepo;

    @Test
    public void deveRetornar200ConsultandoAcessoEntity () throws  Exception {
        URI uri = new URI("/api/acesso/todos");

        mockMvc
                .perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status()
                        .is(200)
                );
    }

    @Test
    private void deveSalvarRepo() {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("cli");
        cliente.setDataNascimento(new Date(2000, 10, 11));
        cliente.setCpf("12345678901");

        clienteRepo.save(cliente);
        //

        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("um");
        espaco.setQtdPessoas(16);
        espaco.setValor(199);

        espacoRepo.save(espaco);
        //

        SaldoClienteID saldoClienteID = new SaldoClienteID();
        saldoClienteID.setIdCliente(clienteRepo.findByCpf("12345678901").getId());
        saldoClienteID.setIdCliente(espacoRepo.findByQtdPessoas(16).getId());


        SaldoClienteEntity saldo = new SaldoClienteEntity();
        saldo.setId(saldoClienteID);
        saldo.setQuantidade(12);
        saldo.setDataVencimento(new Date(2020, 10, 10));
        saldo.setTipoContratacao(TipoContratacaoEnum.TURNO);

        saldoClienteRepo.save(saldo);

        AcessoEntity acesso = new AcessoEntity();
        acesso.setSaldoCliente(saldoClienteRepo.findByQuantidade(12));
        acesso.setData(new Date(2020, 10, 10));

        acessoRepo.save(acesso);

        assertNotNull(clienteRepo.findByCpf("12345678901"));

        assertNotNull(acessoRepo.findAll());

    }


}