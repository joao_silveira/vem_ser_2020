package com.dbccompany.coworking.Repository;

import com.dbccompany.coworking.Entity.ContatoEntity;
import com.dbccompany.coworking.Entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class ContatoRepositoryTest {
    @Autowired
    private TipoContatoRepository tipoContato;

    @Autowired
    private ContatoRepository contatoRepository;

    @Test
    public void criaContato() {
        TipoContatoEntity email = new TipoContatoEntity();
        email.setNome("email");
        tipoContato.save(email);

        ContatoEntity ct = new ContatoEntity();
        ct.setTipo(email);
        ct.setValor("ruvricius@terra.com");
        contatoRepository.save(ct);

        assertEquals(1, contatoRepository.findById(1).get().getId());
    }

    @Test
    public void buscaContatoPorTipo() {
        TipoContatoEntity email = new TipoContatoEntity();
        email.setNome("email");
        tipoContato.save(email);

        ContatoEntity ct = new ContatoEntity();
        ct.setTipo(email);
        ct.setValor("ruvricius@terra.com");
        contatoRepository.save(ct);

        assertEquals(1, contatoRepository.findAllByTipo(email).size());
    }

}
