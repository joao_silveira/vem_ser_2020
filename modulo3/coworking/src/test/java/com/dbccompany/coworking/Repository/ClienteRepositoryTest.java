package com.dbccompany.coworking.Repository;

import com.dbccompany.coworking.Entity.ClienteEntity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
//ClienteEntity findByCpf(String cpf );
public class ClienteRepositoryTest {

    @Autowired
    private ClienteRepository clienteRepo;

    @BeforeEach
    private void init() {
        ClienteEntity cl = new ClienteEntity();
        cl.setCpf("12345678901");
        cl.setNome("Ariovaldino galisteu");
        cl.setDataNascimento(new Date(2000,06,28));

        clienteRepo.save(cl);

    }

    @AfterEach
    private void end() {
        clienteRepo.deleteAll();
    }

    @Test
    public void procuraPorCPF() {
        ClienteEntity cli = clienteRepo.findByCpf("12345678901");

        assertEquals(cli.getNome(), clienteRepo.findByCpf("12345678901").getNome());
    }

    @Test
    public void procuraPorClienteInexistente() {
        assertEquals(null, clienteRepo.findByCpf("12345678123"));
    }

}