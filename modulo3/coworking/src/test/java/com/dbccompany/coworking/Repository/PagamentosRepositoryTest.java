package com.dbccompany.coworking.Repository;

import com.dbccompany.coworking.Entity.PagamentoEntity;
import com.dbccompany.coworking.Entity.TipoPagamentoEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class PagamentosRepositoryTest {

    @Autowired
    private PagamentoRepository repo;

    @Test
    public void buscarPagamentosPorTipo() {
        PagamentoEntity pagamento = new PagamentoEntity();

        pagamento.setTipoPagamento(TipoPagamentoEnum.DEBITO);
        repo.save(pagamento);

        assertEquals(repo.findAllByTipoPagamento(TipoPagamentoEnum.DEBITO).size(), 1);
    }
}
