package com.logs.RepositoryTest;

import com.logs.Entity.LogEntity;
import com.logs.Repository.LogRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest //(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LogRepositoryTest {

    @Autowired
    private LogRepository logRepo;

    @Test
    public void exmapleTest() {
        LogEntity log = new LogEntity();
        log.setMensagem("testeste");

        logRepo.save(log);

        assertNotNull(logRepo.findByMensagem("testeste"));

    }

}
