package com.logs.Service;

import com.logs.Entity.LogEntity;
import com.logs.Repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class LogService {

    @Autowired
    LogRepository repo;

    @Transactional
    public LogEntity salvar(LogEntity log) {
        try {
            return repo.save(log);
        } catch (Exception err) {
            throw new RuntimeException();
        }
    }

    @Transactional
    public List<LogEntity> todos() {
        try {
            List<LogEntity> logs = new ArrayList<>();
            for (LogEntity logEntity: repo.findAll()){
                logs.add(logEntity);
            }
            return logs;
        } catch (Exception err) {
            throw new RuntimeException();
        }
    }

    @Transactional
    public LogEntity logPorCodigoDeErro(String codigo) {
        try {
            LogEntity log = repo.findAllByCodigo(codigo);

            return log;
        } catch (Exception err) {
            throw new RuntimeException();
        }
    }



}
