package com.logs.Controller;

import com.logs.Entity.LogEntity;
import com.logs.Service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/log")
public class LogController {

    @Autowired
    LogService service;

    @GetMapping(value = "/novo")
    @ResponseBody
    public LogEntity salvar(@RequestBody LogEntity log){
        return service.salvar(log);
    }

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<LogEntity> todos(){
        return service.todos();
    }

    @GetMapping(value = "/{id}}")
    @ResponseBody
    public LogEntity logPorCodigoDeErro(String codigo){
        return service.logPorCodigoDeErro(codigo);
    }
}
