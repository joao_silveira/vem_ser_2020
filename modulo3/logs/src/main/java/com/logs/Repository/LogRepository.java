package com.logs.Repository;

import com.logs.Entity.LogEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogRepository extends MongoRepository<LogEntity, String> {
    LogEntity findByMensagem(String mensagem);
    LogEntity findAllByCodigo(String codigo);
}
