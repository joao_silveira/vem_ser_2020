package br.com.dbccompany.vemser.DTO;

import br.com.dbccompany.vemser.Entity.EstadoEntity;

import java.util.List;

public class PaisDTO {

    private Integer id;
    private String nome;
    private List<EstadoEntity> estados;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<EstadoEntity> getEstados() {
        return estados;
    }

    public void setEstados(List<EstadoEntity> estados) {
        this.estados = estados;
    }
}
