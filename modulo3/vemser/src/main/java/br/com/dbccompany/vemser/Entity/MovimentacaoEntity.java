package br.com.dbccompany.vemser.Entity;
import javax.persistence.*;

@Entity
public class MovimentacaoEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "MOVIMENTACAO_SEQ", sequenceName = "MOVIMENTACAO_SEQ")
    @GeneratedValue( generator = "MOVIMENTACAO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private double valor;

    @Enumerated( EnumType.STRING )
    private TipoMovimentacaoEnum tipoMovimentacao;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumns({
            @JoinColumn( name = "id_conta"),
            @JoinColumn( name = "id_tipo_conta")
    })
    private ContaEntity conta;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public TipoMovimentacaoEnum getTipoMovimentacao() {
        return tipoMovimentacao;
    }

    public void setTipoMovimentacao(TipoMovimentacaoEnum tipoMovimentacao) {
        this.tipoMovimentacao = tipoMovimentacao;
    }

    public ContaEntity getConta() {
        return conta;
    }

    public void setConta(ContaEntity conta) {
        this.conta = conta;
    }
}
