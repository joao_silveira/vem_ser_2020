package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.CidadeEntity;
import br.com.dbccompany.vemser.Entity.EstadoEntity;
import br.com.dbccompany.vemser.Entity.PaisEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CidadeRepository extends CrudRepository<CidadeEntity, Integer> {

    List<CidadeEntity> findByEstado(EstadoEntity estado);
    CidadeEntity findByNome(String nome);
}

