package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.PaisEntity;
import br.com.dbccompany.vemser.Service.PaisService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/pais")
public class PaisController extends ControllerAbstract<PaisService, PaisEntity, Integer> {
}
