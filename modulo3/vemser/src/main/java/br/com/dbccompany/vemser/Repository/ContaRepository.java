package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.AgenciaEntity;
import br.com.dbccompany.vemser.Entity.ContaEntity;
import br.com.dbccompany.vemser.Entity.TipoContaEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContaRepository extends CrudRepository<ContaEntity, Integer> {

    ContaEntity findByCodigo(int codigo);
    List<ContaEntity> findByAgencia(AgenciaEntity agencia);
    List<ContaEntity> findByTipoConta(TipoContaEntity tipoConta);
}
