package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.*;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EnderecoRepository extends CrudRepository<EnderecoEntity, Integer> {

    EnderecoEntity findByNumero(int numero);
    EnderecoEntity findByAgencia(AgenciaEntity agencia);
    EnderecoEntity findByUsuario(UsuarioEntity usuario);
    List<EnderecoEntity> findByCidade(CidadeEntity cidade);
    List<EnderecoEntity> findAll();
}
